"""
根据需要创建对象是很常见的需求，在一个函数中，我们可以保留一些默认配置，然后根据需要进行修改。
例如，我们有一个创建用户的函数create_user，参数如下
● username （用户名）必填
● password （密码）	必填
● is_admin （是否是管理员）可选，默认为False
● is_active （是否已激活）可选，默认为True
● is_verified （是否已验证）可选，默认为False

create_user("john", "password")
create_user("jane", "password123", is_admin=True)
create_user("alice", "pass123", is_active=False, is_verified=True)
"""


def create_user(username, password, is_admin=False, is_active=True, is_verified=False):
    # 创建用户的逻辑
    print("Creating user...")
    print("Username:", username)
    print("Password:", password)
    print("Is Admin:", is_admin)
    print("Is Active:", is_active)
    print("Is Verified:", is_verified)
    print("User created successfully.")


# 示例用法
create_user("john", "password")
create_user("jane", "password123", is_admin=True)
create_user("alice", "pass123", is_active=False, is_verified=True)
