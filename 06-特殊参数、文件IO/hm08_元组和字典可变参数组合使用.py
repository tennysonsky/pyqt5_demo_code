# **kwargs形参可以与 *args形参组合使用（*args必须在 **kwargs前面）
def milk_tea_shop(kind, *arguments, **keywords):
    print("-- 老板，给我来杯", kind, "！")
    print("-- 对不起，我们的 %s 已经卖完啦！" % kind)
    for arg in arguments:
        print(arg)
    print("-" * 40)
    for kw in keywords:
        print(kw, ":", keywords[kw])


# 函数的调用
milk_tea_shop(
    "QQ咩咩好喝到爆的咩噗茶",
    "加糖", "少冰", "加奶", "加珍珠",
    price="10元", address="北京市朝阳区望京SOHO",
    phone="010-12345678"
)
