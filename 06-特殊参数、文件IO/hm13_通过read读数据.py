"""
# 1. 打开文件，只读方式打开，'r'
# 'r'： 打开文件，必须存在，不存在，报错崩溃
# 2. 读取文件内容
# 格式： 内容变量 = 文件变量.read(读取的长度)
#       如果read的长度不指定，默认读取全部
"""

with open('u1s1.txt', 'r', encoding='utf-8') as f:
    # 先读5个字符
    data = f.read(5)
    print(f'data = #{data}#')
    # 再读3个字符
    data = f.read(3)
    print(f'data = #{data}#')
    # 读剩下的全部
    data = f.read()
    print(f'data = #{data}#')
