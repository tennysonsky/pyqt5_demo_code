"""
1.输入文件名 文件名.py
2.创建文件  文件名[复制].py
3.读取文件, 写入到复制的文件中
"""

filename = input("请输入文件名：")

dot_index = filename.rfind(".")
name_prefix = filename[:dot_index]
name_suffix = filename[dot_index:]

# 新文件的名字
new_filename = name_prefix + "[复制]" + name_suffix

with open(filename, "r", encoding="utf-8") as f:
    content = f.read()

with open(new_filename, "w", encoding="utf-8") as f:
    f.write(content)

print("拷贝完成")
