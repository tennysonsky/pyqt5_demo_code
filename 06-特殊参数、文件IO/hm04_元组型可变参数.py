"""
说明:
    如果一个函数能够处理的参数个数是不确定的, 可以考虑使用可变参数(不定长参数)
语法：
    1. 函数形参变量的前面加一个*， 这个参数则为元组型不定长参数
    2. *args : 用于接收元组类型数据, args 为习惯命名, 可以自定义
    3. args 是 arguments 的缩写, 有变量的含义
    4. 在函数内使用形参，无需加*
    5. 函数调用，元组型不定长参数使用位置传参
"""


# 函数形参变量的前面加一个*， 这个参数则为元组型不定长参数
# args能接收0~n实参
def func(*args):
    print(f'args = {args}, 类型：{type(args)}')


# 函数调用，元组型不定长参数使用位置传参
func()  # args = (), 类型：<class 'tuple'>
func('mike')  # args = ('mike',), 类型：<class 'tuple'>
func('mike', 18)  # args = ('mike', 18), 类型：<class 'tuple'>
func('mike', 18, 'sz')  # args = ('mike', 18, 'sz'), 类型：<class 'tuple'>
