"""
需求：
1. 输入一个文件名，统计文件中代码行数、注释行数、空行数
2. 打印代码行数、注释行数、空行数
分析：
1.输入文件名 test.py
2.打开文件
3.统计 readline
    空行 空
    注释行数 去空格 #开头
    代码行数
"""

filename = input("请输入文件名：")

blank_lines = 0  # 空行个数
comment_lines = 0  # 注释行数
code_lines = 0  # 代码行数

with open(filename, "r", encoding="utf-8") as f:
    while True:
        line = f.readline()
        if line: 
            line = line.strip()  # 去空
            if line == "":
                blank_lines += 1
            elif line.startswith("#"):
                comment_lines += 1
            else:
                code_lines += 1
        else:
            print("文件读取完毕")
            break

print("代码行数：{} 注释行数：{} 空行数：{}".format(code_lines, comment_lines, blank_lines))
