"""操作文件的流程：
1. 打开文件
2. 读或写文件
3. 关闭文件

打开文件格式：
    文件变量 = open(文件名字，访问模式, encoding='utf-8')
注意：
1. 文件名字，访问模式都是字符串类型
2. 如果操作文本文件，encoding='utf-8'为指定utf-8编码，防止不同平台中文乱码
    - 这个当做结论记
"""

# 'w': write 只写方式打开文件，文件不存在创建，文件存在，清空覆盖文件内容
# 1. 文件名可以带上路径
# f = open('../u1s1.txt', 'w', encoding='utf-8')
# 'r': read 只读方式打开文件，如果文件不存在，报错
# f = open('u1s1.txt', 'r', encoding='utf-8')
# 2. 默认访问模式为 'r'
f = open('u1s1.txt', encoding='utf-8')
# f = open('u1s1xxxx.txt', 'r', encoding='utf-8')

# 关闭文件，为了释放资源
# 文件变量.close()
f.close()
