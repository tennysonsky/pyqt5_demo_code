"""
# 只要文件打开了，运行完with语句，不管是否发生异常都会自动关闭文件
# with open() as 文件变量名：
#   文件的操作
"""

with open('u1s1.txt', 'w', encoding='utf-8') as f:
    print('后续要写文件')
