"""
案例是求n的阶乘，如：5的阶乘：5*4*3*2*1
分析：
1. 如果传递的是1，则1的阶乘为1
2. 如果传递的是大于1的数，n的阶乘就等于n乘以n-1的阶乘
"""


def factorial(n):
    if n == 1:
        return 1
    else:
        return n * factorial(n - 1)


# 函数调用
print(factorial(5))
