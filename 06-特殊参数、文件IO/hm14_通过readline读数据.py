"""
# readline 方法可以一次读取一行内容, 想要读取完全部内容, 一般会配合循环使用!
# 1. 只读方式打开文件
# 2. 死循环
# 3. 读取1行内容，data接收返回值
# 4. if data:  有内容，就是真，非0，非空字符串''，就是真，打印读取的内容
# 5. 否则，说明没有读到内容，说明文件读取完毕，跳出循环
"""

# 1. 只读方式打开文件
with open('u1s1.txt', 'r', encoding='utf-8') as f:
    # 2. 死循环
    while True:
        # 3. 读取1行内容，data接收返回值
        data = f.readline()
        # 4. if data:  有内容，就是真，非0，非空字符串''，就是真，打印读取的内容
        if data:
            # data中的数据把'\n'换成''，重新给data
            data = data.replace('\n', '')
            print(f'data = #{data}#')
        # 5. 否则，说明没有读到内容，说明文件读取完毕，跳出循环
        else:
            print('文件读取完毕')
            break
