print("==========元组型可变参数===========")
def tuple_func(*args):
    print(f'args = {args}, 类型：{type(args)}')


# 函数调用
temp = (1, 2, 3, 4)
# 在调用函数参数时, 实参加 * , 代表把实参的内容拆分后再传递，为拆包操作
tuple_func(*temp)

print("==========字典型可变参数===========")
def kw_func(**kwargs):
    print(f'kwargs = {kwargs}, 类型：{type(kwargs)}')


# 函数调用， 实参加 **, 代表把实参的内容拆分分后再传递，为拆包操作
temp = {'a': 1, 'b': 2, 'c': 3}
kw_func(**temp)
