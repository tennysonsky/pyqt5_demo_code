"""
# 需求:
# 1. 定义一个函数 sum_numbers， 可以接收的 任意多个整数
# 2. 功能要求： 将传递的 所有数字累加 并且返回累加结果
"""

def sum_numbers(*args):
    # args = (1, 3, 5, 7, 9)
    # print(f'args = {args}')
    n = 0
    for v in args:
        # print(f'v = {v}')
        n += v

    return n


# 函数调用
temp = sum_numbers(1, 3, 5, 7, 9)
print(temp)
