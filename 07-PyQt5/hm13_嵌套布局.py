"""
# 创建QHBoxLayout对象h_layout
# 创建QVBoxLayout对象v_layout1，添加一个QPushButton对象1
# h_layout添加v_layout1对象
# 创建QVBoxLayout对象v_layout2，添加QPushButton对象2, QPushButton对象3
# h_layout添加v_layout2对象
# 创建QVBoxLayout对象v_layout3，添加QPushButton对象4, QPushButton对象5, QPushButton对象6
# h_layout添加v_layout3对象
# 创建QVBoxLayout对象v_layout4，添加QPushButton对象7, QPushButton对象8, QPushButton对象9, QPushButton对象10
# h_layout添加v_layout4对象
# 窗口添加布局h_layout
"""

from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout, QPushButton
import sys

# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()

# 创建QHBoxLayout对象h_layout
h_layout = QHBoxLayout()

# 创建QVBoxLayout对象v_layout1，添加一个QPushButton对象1
v_layout1 = QVBoxLayout()
v_layout1.addWidget(QPushButton('1'))
# h_layout添加v_layout1对象
h_layout.addLayout(v_layout1)

# 创建QVBoxLayout对象v_layout2，添加QPushButton对象2, QPushButton对象3
v_layout2 = QVBoxLayout()
v_layout2.addWidget(QPushButton('2'))
v_layout2.addWidget(QPushButton('3'))
# h_layout添加v_layout2对象
h_layout.addLayout(v_layout2)

# 创建QVBoxLayout对象v_layout3，添加QPushButton对象4, QPushButton对象5, QPushButton对象6
v_layout3 = QVBoxLayout()
v_layout3.addWidget(QPushButton('4'))
v_layout3.addWidget(QPushButton('5'))
v_layout3.addWidget(QPushButton('6'))
# h_layout添加v_layout3对象
h_layout.addLayout(v_layout3)

# 创建QVBoxLayout对象v_layout4，添加QPushButton对象7, QPushButton对象8, QPushButton对象9, QPushButton对象10
v_layout4 = QVBoxLayout()
v_layout4.addWidget(QPushButton('7'))
v_layout4.addWidget(QPushButton('8'))
v_layout4.addWidget(QPushButton('9'))
v_layout4.addWidget(QPushButton('10'))
# h_layout添加v_layout4对象
h_layout.addLayout(v_layout4)

# 窗口添加布局h_layout
w.setLayout(h_layout)

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
