"""
# 设置窗口标题
# 设置窗口尺寸
# 设置图标
# 鼠标停留在控件上时就会显示ToolTip的信息
"""
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QWidget
import sys

# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()

# 设置窗口标题
w.setWindowTitle("黑马窗口")
# 设置窗口尺寸
w.resize(640, 480)
# 设置图标
w.setWindowIcon(QIcon("img/qq.png"))
# 鼠标停留在控件上时就会显示ToolTip的信息
w.setToolTip("这是一个窗口")

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
