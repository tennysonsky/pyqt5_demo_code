from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("./img/qq.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Form.setWindowIcon(icon)
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton = QtWidgets.QPushButton(Form)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 0, 0, 1, 1)
        self.labelText = QtWidgets.QLabel(Form)
        self.labelText.setObjectName("labelText")
        self.gridLayout.addWidget(self.labelText, 1, 0, 1, 1)
        self.labelImage = QtWidgets.QLabel(Form)
        self.labelImage.setText("")
        self.labelImage.setPixmap(QtGui.QPixmap("./img/face.png"))
        self.labelImage.setScaledContents(True)
        self.labelImage.setObjectName("labelImage")
        self.gridLayout.addWidget(self.labelImage, 2, 2, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(Form)
        self.lineEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 0, 1, 1, 2)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "练习标题"))
        self.pushButton.setText(_translate("Form", "登录"))
        self.labelText.setText(_translate("Form", "测试文字显示情况"))
