"""
# 创建QPushButton对象btn
# btn信号clicked绑定槽函数
# 槽函数中弹出消息QMessageBox对话框，并设置消息对话框的标题、内容、按钮，选择对话框后做出响应判断
# btn指定父对象
"""

from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QMessageBox
import sys

# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()


# 槽函数中弹出消息QMessageBox对话框，并设置消息对话框的标题、内容、按钮，选择对话框后做出响应判断
def show_dialog():
    res = QMessageBox.information(w, "标题", "内容", QMessageBox.Yes | QMessageBox.No)

    if res == QMessageBox.Yes:
        print("Yes")
    else:
        print("No")


# 创建QPushButton对象btn
btn = QPushButton("点我弹出消息对话框")
# btn信号clicked绑定槽函数
btn.clicked.connect(show_dialog)
# btn指定父对象
btn.setParent(w)

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
