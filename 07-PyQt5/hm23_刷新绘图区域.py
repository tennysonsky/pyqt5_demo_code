import sys

from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from ui.Ui_event_widget import Ui_Form


class MyWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        # 实例属性
        self.press_i = -1
        self.press_j = -1
        self.grid_width = 1
        self.grid_height = 1
        self.start_x = 0
        self.start_y = 0

        # 设置窗口大小
        self.resize(800, 800)

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        pass

    def paintEvent(self, event: QPaintEvent):  # 绘图事件
        # 调用父类同名方法
        super().paintEvent(event)

        # 定义画家（请了一个画家来画画），画在主窗口上
        painter = QPainter(self)

        # 以窗口大小画背景图
        painter.drawPixmap(
            0, 0, self.width(), self.height(), QPixmap("./img/board.jpg")
        )

        # 窗口宽度分12份
        grid_width = self.width() // 12
        # 窗口高度分12份
        grid_height = self.height() // 12

        # 棋盘起点坐标为grid_width， grid_height
        start_x = grid_width * 2
        start_y = grid_height * 2

        # 实例属性修改
        self.grid_width = grid_width
        self.grid_height = grid_height
        self.start_x = start_x
        self.start_y = start_y

        # 设置线宽为2
        line_width = 4
        painter.setPen(QPen(Qt.black, line_width))

        # 取中间8份画棋盘
        for i in range(9):
            # 画横线
            painter.drawLine(
                start_x,
                start_y + grid_height * i,
                start_x + grid_width * 8,
                start_y + grid_height * i,
            )
            # 画竖线
            painter.drawLine(
                start_x + grid_width * i,
                start_y,
                start_x + grid_width * i,
                start_y + grid_height * 8,
            )

        if self.press_i == -1:
            return

        # 在棋盘格子中，画一个棋子图片
        painter.drawPixmap(
            start_x + grid_width * self.press_i,
            start_y + grid_height * self.press_j,
            grid_width - line_width,
            grid_height - line_width,
            QPixmap("./img/white.png"),
        )

        # 设置画笔颜色为绿色
        painter.setPen(QPen(Qt.green, line_width))
        # 画矩形
        painter.drawRect(
            start_x + grid_width * self.press_i,
            start_y + grid_height * self.press_j,
            grid_width,
            grid_height,
        )

    def mousePressEvent(self, event: QMouseEvent):  # 鼠标按下事件
        # 调用父类同名方法
        super().mousePressEvent(event)
        # 获取 x, y 坐标
        x = event.x()
        y = event.y()

        # 范围判断，点击范围不要超过棋盘
        if (
            x >= self.start_x
            and x <= self.start_x + self.grid_width * 8
            and y >= self.start_y
            and y <= self.start_y + self.grid_height * 8
        ):
            self.press_i = (x - self.start_x) // self.grid_width
            self.press_j = (y - self.start_y) // self.grid_height
            print(self.press_i, self.press_j)

            self.update()  # 更新绘图区域


if __name__ == "__main__":
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.show()

    sys.exit(app.exec_())
