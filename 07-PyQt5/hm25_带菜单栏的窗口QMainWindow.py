"""
1. 菜单按钮triggered信号绑定槽函数
2. 工具栏添加菜单的动作
3. 状态栏显示信息
"""

import sys
from PyQt5.QtWidgets import *
from ui.Ui_main_window import Ui_MainWindow


class MainWindow(QMainWindow):
    """注意：父类必须是QMainWindow"""
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        # 1. 菜单按钮triggered信号绑定槽函数
        self.ui.actionnew.triggered.connect(lambda: print("新建"))
        self.ui.actionsave.triggered.connect(lambda: print("保存"))
        
        # 2. 工具栏添加菜单的动作
        self.ui.toolBar.addAction(self.ui.actionnew)
        self.ui.toolBar.addAction(self.ui.actionsave)
        
        # 3. 状态栏显示信息
        self.ui.statusbar.showMessage("这是状态栏显示的文字信息")


if __name__ == '__main__':
    app = QApplication(sys.argv)

    W = MainWindow()
    W.show()

    sys.exit(app.exec_())