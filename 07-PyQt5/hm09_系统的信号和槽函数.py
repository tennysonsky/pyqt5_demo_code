from PyQt5.QtWidgets import QApplication, QWidget, QPushButton
import sys


def btn_click_func():
    """槽函数"""
    print("按钮被点击了")


# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()

# 创建QPushButton对象
btn = QPushButton()
# 设置按钮的文本内容
btn.setText("点我啊^_^")
# 获取按钮的文本内容
print(btn.text())
# 指定父对象
btn.setParent(w)

# 系统的槽函数，不用加()，直接调用
# 按钮被点击了，调用关闭窗口的槽函数
btn.clicked.connect(w.close)

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
