"""
# 导包中模块 from ui.ui模块文件名 import Ui_对象名
# 实例化ui模块对象，调用setupUi()方法
"""

from PyQt5.QtWidgets import QWidget, QApplication
import sys
from ui.Ui_form import Ui_Form


class MyWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        # 初始化UI界面
        self.init_ui()

    def btn_click(self):
        # 如果密码框为空，标签显示‘密码不能为空’
        if self.ui.lineEdit.text() == '':
            self.ui.labelText.setText('密码不能为空')
        else:  # 否则，标签密码内容
            self.ui.labelText.setText(self.ui.lineEdit.text())

    def init_ui(self):
        # 按钮信号和槽关联
        self.ui.pushButton.clicked.connect(self.btn_click)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.show()

    sys.exit(app.exec_())
