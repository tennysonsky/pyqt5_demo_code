import sys

from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from ui.Ui_event_widget import Ui_Form


class MyWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        # 设置窗口大小
        self.resize(800, 800)

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        pass

    def paintEvent(self, event: QPaintEvent):  # 绘图事件
        # 调用父类同名方法
        super().paintEvent(event)

        # 定义画家（请了一个画家来画画），画在主窗口上
        painter = QPainter(self)

        # 以窗口大小画背景图
        painter.drawPixmap(
            0, 0, self.width(), self.height(), QPixmap("./img/board.jpg")
        )

        # 窗口宽度分12份
        grid_width = self.width() // 12
        # 窗口高度分12份
        grid_height = self.height() // 12

        # 棋盘起点坐标为grid_width， grid_height
        start_x = grid_width * 2
        start_y = grid_height * 2

        # 设置线宽为4
        line_width = 4
        painter.setPen(QPen(Qt.black, line_width))

        # 取中间8份画棋盘
        for i in range(9):
            # 画横线
            painter.drawLine(
                start_x,
                start_y + grid_height * i,
                start_x + grid_width * 8,
                start_y + grid_height * i,
            )
            # 画竖线
            painter.drawLine(
                start_x + grid_width * i,
                start_y,
                start_x + grid_width * i,
                start_y + grid_height * 8,
            )

        # 在棋盘格子中，画一个棋子图片
        for i in range(8):
            for j in range(8):
                painter.drawPixmap(
                    start_x + grid_width * i,
                    start_y + grid_height * j,
                    grid_width,
                    grid_height,
                    QPixmap("./img/white.png"),
                )


if __name__ == "__main__":
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.show()

    sys.exit(app.exec_())
