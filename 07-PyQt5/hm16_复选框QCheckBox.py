"""
# 创建QCheckBox对象box1,并设置文本为“学习”
# 创建QCheckBox对象box2,并设置文本为“睡觉”
# 创建QCheckBox对象box3,并设置文本为“工作”
# box1、box2、box3的信号statusChanged和槽函数box_slot绑定
# 槽函数判断是否被选中
# 创建QHBoxLayout对象layout,并添加box1、box2、box3
# 窗口添加布局
"""

from PyQt5.QtWidgets import QApplication, QWidget, QCheckBox, QHBoxLayout
import sys

# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()


# 槽函数判断是否被选中
def box_slot():
    if box1.isChecked():
        print("学习")
    if box2.isChecked():
        print("睡觉")
    if box3.isChecked():
        print("工作")


# 创建QCheckBox对象box1,并设置文本为“学习”
box1 = QCheckBox("学习")
# 创建QCheckBox对象box2,并设置文本为“睡觉”
box2 = QCheckBox("睡觉")
# 创建QCheckBox对象box3,并设置文本为“工作”
box3 = QCheckBox("工作")
# box1、box2、box3的信号statusChanged和槽函数box_slot绑定
box1.stateChanged.connect(box_slot)
box2.stateChanged.connect(box_slot)
box3.stateChanged.connect(box_slot)
# 创建QHBoxLayout对象layout,并添加box1、box2、box3
layout = QHBoxLayout()
layout.addWidget(box1)
layout.addWidget(box2)
layout.addWidget(box3)
# 窗口添加布局
w.setLayout(layout)

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
