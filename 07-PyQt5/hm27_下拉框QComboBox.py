"""
# 追加选项
# 清空选项
# 设置显示第几个选项，计算从0开始
# 返回选项的数目
# 信号currentIndexChanged和槽绑定
"""

import sys
from PyQt5.QtWidgets import *
from ui.Ui_main_window import Ui_MainWindow


class MainWindow(QMainWindow):

    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        # ======================= 菜单栏、工具栏、状态栏
        # 1. 菜单按钮triggered信号绑定槽函数
        self.ui.actionnew.triggered.connect(lambda: print("新建"))
        self.ui.actionsave.triggered.connect(lambda: print("保存"))
        
        # 2. 工具栏添加菜单的动作
        self.ui.toolBar.addAction(self.ui.actionnew)
        self.ui.toolBar.addAction(self.ui.actionsave)
        
        # 3. 状态栏显示信息
        self.ui.statusbar.showMessage("这是状态栏显示的文字信息")
        
        # ======================= 标签容器QTabWidget
        # 标签容器QTabWidget添加新窗口
        self.ui.tabWidget.addTab(QWidget(self), "串口调试助手")
        self.ui.tabWidget.addTab(QWidget(self), "蓝牙调试助手")
        # 设置显示第几个页面，从0开始计算
        self.ui.tabWidget.setCurrentIndex(0)
        
        # ======================= 下拉框QComboBox
        # 追加选项
        self.ui.comboBox.addItem("选项1") # addItem
        self.ui.comboBox.addItem("选项2")
        self.ui.comboBox.addItems(["选项3", "选项4"]) # addItems 有个s
        # 清空选项
        # self.ui.comboBox.clear()
        # 设置显示第几个选项，计算从0开始
        self.ui.comboBox.setCurrentIndex(1)
        # 返回当前显示内容
        print(self.ui.comboBox.currentText())
        # 返回选项的数目
        print(self.ui.comboBox.count())
        # 信号currentIndexChanged和槽绑定
        self.ui.comboBox.currentIndexChanged.connect(self.currentIndexChangedSlot)
        
    def currentIndexChangedSlot(self, index):
        """信号currentIndexChanged和槽绑定"""
        # 选择下拉框时，此处会触发
        print(index)

if __name__ == '__main__':
    app = QApplication(sys.argv)

    W = MainWindow()
    W.show()

    sys.exit(app.exec_())