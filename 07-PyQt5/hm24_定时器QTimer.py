"""
# 导包中模块 from ui.ui模块文件名 import Ui_对象名
# 实例化ui模块对象，调用setupUi()方法
"""
import sys

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from ui.Ui_timer_widgt import Ui_Form



class MyWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        
        # 实例属性
        self.timer = QTimer() # 实例化定时器
        self.num = 0    # 计数器
        
        # 定时器信号和槽连接
        self.timer.timeout.connect(self.timerSlot)

        # 初始化UI界面
        self.init_ui()

    def btnStartSlot(self):
        print("开始按钮被点击了")
        # 如果定时器没有启动
        if not self.timer.isActive():
            # 启动定时器
            self.timer.start(500) # 单位为毫秒
        
    def btnStopSlot(self):
        print("停止按钮被点击了")
        # 如果定时器启动了
        if self.timer.isActive():
            # 停止定时器
            self.timer.stop()
        
    def timerSlot(self):
        # 计数器自增
        self.num += 1
        # 数码管显示
        self.ui.lcdNumber.display(self.num)

    def init_ui(self):
        # 信号和槽关联
        self.ui.btnStart.clicked.connect(self.btnStartSlot)
        self.ui.btnStop.clicked.connect(self.btnStopSlot)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.show()

    sys.exit(app.exec_())
