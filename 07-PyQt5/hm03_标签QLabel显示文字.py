"""
# 创建QLabel标签对象
# 设置文本内容
# 指定主窗口为父对象，标签放在主窗口上
"""
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QWidget, QLabel
import sys

# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()
# 设置窗口标题
w.setWindowTitle("黑马窗口")

# 创建QLabel标签对象
label = QLabel()
# 设置文本内容
label.setText("这是一个QLabel标签")
# 指定主窗口为父对象，标签放在主窗口上
label.setParent(w)

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
