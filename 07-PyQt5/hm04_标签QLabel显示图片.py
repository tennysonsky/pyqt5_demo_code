"""
# 创建QLabel标签对象
# 创建QPixmap对象
# 设置标签图片
# 指定主窗口为父对象，标签放在主窗口上
# 根据图片大小设置窗口大小
"""
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QWidget, QLabel
import sys

# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()
# 设置窗口标题
w.setWindowTitle("黑马窗口")

# 创建QLabel标签对象
label = QLabel()
# 创建QPixmap对象
pixmap = QPixmap("img/img.png")
# 设置标签图片
label.setPixmap(pixmap)
# 指定主窗口为父对象，标签放在主窗口上
label.setParent(w)
# 根据图片大小设置窗口大小
w.resize(pixmap.width(), pixmap.height())

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
