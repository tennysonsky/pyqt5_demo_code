"""
类名：MyWidget继承QWidget
实例方法：
    __init__(self, parent=None)： 调用父类的__init__(parent)方法，调用初始化窗口
    init_ui(): 窗口控件的添加、布局、信号和槽处理

程序入口：
    # 1.创建应用程序
    # 2.创建窗口
    # 3.显示窗口
    # 4.等待窗口停止
"""

from PyQt5.QtWidgets import *
import sys


class MyWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("自定义窗口标题")
        self.resize(640, 480)

        # 初始化UI界面
        self.init_ui()

    def btn1_clicked(self):
        print("按钮1被点击")

    def btn2_clicked(self):
        print("按钮2被点击")

    def init_ui(self):
        # 添加两个按钮
        # 给按钮绑定事件
        layout = QVBoxLayout()
        # ---------------------------------

        # 在这里初始化界面内容
        btn1 = QPushButton("按钮1")
        btn2 = QPushButton("按钮2")
        btn1.clicked.connect(self.btn1_clicked)
        btn2.clicked.connect(self.btn2_clicked)
        layout.addWidget(btn1)
        layout.addWidget(btn2)
        # ---------------------------------
        self.setLayout(layout)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.show()

    sys.exit(app.exec_())
