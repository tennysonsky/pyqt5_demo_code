"""
# 创建QHBoxLayout对象
# 添加5个QPushButton控件
# 窗口添加布局
"""

from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QPushButton
import sys

# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()

# 创建QHBoxLayout对象
layout = QHBoxLayout()
# 添加5个QPushButton控件
for i in range(5):
    btn = QPushButton(str(i))
    layout.addWidget(btn)
# 窗口添加布局
w.setLayout(layout)

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
