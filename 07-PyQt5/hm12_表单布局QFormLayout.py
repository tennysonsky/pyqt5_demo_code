"""
# 创建QFormLayout对象
# 创建用户名QLineEdit对象
# 创建密码QLineEdit对象
# 密码QLineEdit对象设置密码属性
# 创建手机号QLineEdit对象
# QFormLayout对象添加用户名、密码、手机号控件
# 创建QPushButton对象
# QPushButton对象信号clicked绑定自定义槽函数on_submit
# 槽函数on_submit实现功能：分别获取用户名、密码、手机号文本内容
# QFormLayout对象添加按钮
# 窗口设置布局
"""

from PyQt5.QtWidgets import QApplication, QWidget, QFormLayout, QLineEdit, QPushButton
import sys

# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()


# 槽函数on_submit实现功能：分别获取用户名、密码、手机号文本内容
def on_submit():
    print("用户名：", usernameLineEdit.text())
    print("密码：", passwordLineEdit.text())
    print("手机号：", mobileLineEdit.text())


# 创建QFormLayout对象
formLayout = QFormLayout()
# 创建用户名QLineEdit对象
usernameLineEdit = QLineEdit()
# 创建密码QLineEdit对象
passwordLineEdit = QLineEdit()
# 密码QLineEdit对象设置密码属性
passwordLineEdit.setEchoMode(QLineEdit.Password)
# 创建手机号QLineEdit对象
mobileLineEdit = QLineEdit()

# QFormLayout对象添加用户名、密码、手机号控件
formLayout.addRow("用户名:", usernameLineEdit)
formLayout.addRow("密码:", passwordLineEdit)
formLayout.addRow("手机号:", mobileLineEdit)

# 创建QPushButton对象
submitButton = QPushButton("提交")
# QPushButton对象信号clicked绑定自定义槽函数on_submit
submitButton.clicked.connect(on_submit)

# QFormLayout对象添加按钮
formLayout.addRow("", submitButton)
# 窗口设置布局
w.setLayout(formLayout)

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
