"""
# 创建QLineEdit对象
# 设置默认提示语
# 设置内容为"hello world"
# 设置最大输入的长度
# 获取文本内容
# 设置输入框的回显模式：密码模式
# 指定父对象
"""

from PyQt5.QtWidgets import QApplication, QWidget, QLineEdit
import sys

# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()

# 创建QLineEdit对象
lineEdit = QLineEdit()
# 设置默认提示语
lineEdit.setPlaceholderText("请输入姓名")
# 设置内容为"hello world"
lineEdit.setText("hello world")
# 设置最大输入的长度
lineEdit.setMaxLength(10)
# 获取文本内容
print(lineEdit.text())
# 设置输入框的回显模式：密码模式
lineEdit.setEchoMode(QLineEdit.Password)
# 指定父对象
lineEdit.setParent(w)

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
