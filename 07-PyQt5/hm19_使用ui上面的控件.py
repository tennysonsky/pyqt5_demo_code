"""
# 导包中模块 from ui.ui模块文件名 import Ui_对象名
# 实例化ui模块对象，调用setupUi()方法
"""

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QApplication
import sys
from ui.Ui_my_widget import Ui_Form


class MyWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        # 初始化UI界面
        self.init_ui()

    def button_clicked(self):
        print("按钮被点击了")

    def init_ui(self):
        # 给pushButton重新设置内容
        self.ui.pushButton.setText("我是pushButton")
        # 信号和槽关联
        self.ui.pushButton.clicked.connect(self.button_clicked)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.show()

    sys.exit(app.exec_())
