import sys

from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from ui.Ui_event_widget import Ui_Form


class MyWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        pass
    
    """
    def mousePressEvent(self, event: QMouseEvent): # 鼠标按下事件
        # 调用父类同名方法
        super().mousePressEvent(event)
        # 获取 x, y 坐标
        x = event.x()
        y = event.y()
        
        if event.button() == Qt.LeftButton:
            print('鼠标左键按下，坐标为：', x, y)
        elif event.button() == Qt.RightButton:
            print('鼠标右键按下，坐标为：', x, y)
        elif event.button() == Qt.MidButton:
            print('鼠标中键按下，坐标为：', x, y)
            
    def mouseReleaseEvent(self, event: QMouseEvent): # 鼠标抬起事件
        # 调用父类同名方法
        super().mouseReleaseEvent(event)
        # 获取 x, y 坐标
        x = event.x()
        y = event.y()
        
        if event.button() == Qt.LeftButton:
            print('鼠标左键抬起，坐标为：', x, y)
        elif event.button() == Qt.RightButton:
            print('鼠标右键抬起，坐标为：', x, y)
        elif event.button() == Qt.MidButton:
            print('鼠标中键抬起，坐标为：', x, y)
    """
    
    def mouseDoubleClickEvent(self, event: QMouseEvent): # 鼠标双击事件
        # 调用父类同名方法
        super().mouseDoubleClickEvent(event)
        # 获取 x, y 坐标
        x = event.x()
        y = event.y()
        
        if event.button() == Qt.LeftButton:
            print('鼠标左键双击，坐标为：', x, y)
        elif event.button() == Qt.RightButton:
            print('鼠标右键双击，坐标为：', x, y)
        elif event.button() == Qt.MidButton:
            print('鼠标中键双击，坐标为：', x, y)
    
    
    def mouseMoveEvent(self, event: QMouseEvent): # 鼠标移动事件
        # 调用父类同名方法
        super().mouseMoveEvent(event)
        # 获取 x, y 坐标
        x = event.x()
        y = event.y()
        
        if event.buttons() == Qt.LeftButton:
            print('鼠标左键移动，坐标为：', x, y)
        elif event.buttons() == Qt.RightButton:
            print('鼠标右键移动，坐标为：', x, y)
        elif event.buttons() == Qt.MidButton:
            print('鼠标中键移动，坐标为：', x, y)
    

if __name__ == '__main__':
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.show()

    sys.exit(app.exec_())