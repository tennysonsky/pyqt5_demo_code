from PyQt5.QtWidgets import *
from ui.Ui_main_widget import Ui_MainWidget
from sub_widget import SubWidget

class MainWidget(QWidget):
    """主窗口"""
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_MainWidget()
        self.ui.setupUi(self)
        
        # 实例属性
        # 实例化子窗口对象，不要指定父对象，2个为独立窗口
        self.sub_widget = SubWidget()
        
        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        # 按键信号和槽连接
        self.ui.btnShow.clicked.connect(self.btn_show_slot)
        self.ui.btnClose.clicked.connect(self.btn_close_slot)
        # 子窗口信号和槽连接
        self.sub_widget.signal1.connect(self.signal1_slot)
        self.sub_widget.signal2.connect(self.signal2_slot)

    def btn_show_slot(self):
        # 显示子窗口
        self.sub_widget.show()
        
    def btn_close_slot(self):
        # 关闭子窗口
        self.sub_widget.close()
        
    def signal1_slot(self, a, b):
        # 子窗口信号槽
        print(f'主窗口处理子窗口信号1  a: {a}, b: {b}')
        
    def signal2_slot(self):
        # 子窗口信号槽
        print('主窗口处理子窗口信号2')