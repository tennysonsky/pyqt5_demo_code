from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from ui.Ui_sub_widget import Ui_SubWidget

"""
1. 定义信号：在类中定义一个信号，使用PyQt的信号机制可以实现自定义信号。可以通过pyqtSignal()方法来创建一个信号对象。 
2. 连接信号和槽：使用connect()方法将信号和槽连接起来。 
3. 触发信号：在需要触发信号的地方，使用emit()方法来触发该信号。 
"""

class SubWidget(QWidget):
    """子窗口"""
    # 信号1，2个参数，至于多少个参数，用户自定义
    signal1 = pyqtSignal(int, str)
    # 信号2，没有参数
    signal2 = pyqtSignal()
    
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_SubWidget()
        self.ui.setupUi(self)

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        # 按钮信号和槽连接
        self.ui.btn1.clicked.connect(self.btn1_clicked_slot)
        self.ui.btn2.clicked.connect(self.btn2_clicked_slot)
        
    def btn1_clicked_slot(self):
        print('btn1_clicked_slot')
        # 发送信号1
        self.signal1.emit(250, "mike")
        
    def btn2_clicked_slot(self):
        print('btn2_clicked_slot')
        # 发送信号2
        self.signal2.emit()

