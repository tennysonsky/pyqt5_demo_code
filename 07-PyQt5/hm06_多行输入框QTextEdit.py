"""
# 创建QTextEdit对象
# 设置提示内容
# 设置默认内容
# 设置内容
# 获取内容
# 清空内容
# 指定父对象
"""

from PyQt5.QtWidgets import QApplication, QWidget, QTextEdit
import sys

# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()

# 创建QTextEdit对象
textEdit = QTextEdit()
# 设置提示内容
textEdit.setPlaceholderText("请输入内容")
# 设置默认内容
textEdit.setPlainText("默认内容")
# 设置内容
textEdit.setText("设置内容")
# 获取内容
text = textEdit.toPlainText()
print(text)
# 清空内容
textEdit.clear()
# 指定父对象
textEdit.setParent(w)

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
