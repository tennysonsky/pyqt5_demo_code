"""
# 创建QRadioButton对象btn1，设置内容"男"
# 创建QRadioButton对象btn2，设置内容"女"
# 设置btn2为选中状态
# 创建QButtonGroup对象group，将btn1和btn2添加到group
# group的信号buttonToggled连接槽函数on_group_toggled
# 槽函数参数为QRadioButton对象，里面打印被按下按钮的文本，打印选中状态
# 创建VBoxLayout对象layout，将btn1和btn2添加到layout
# 窗口添加布局
"""

from PyQt5.QtWidgets import QApplication, QWidget, QRadioButton, QButtonGroup, QVBoxLayout
import sys

# 1.创建应用程序
app = QApplication(sys.argv)

# 2.创建窗口
w = QWidget()


# 槽函数参数为QRadioButton对象，里面打印被按下按钮的文本，打印选中状态
def on_group_toggled(btn):
    print(btn.text(), btn.isChecked())


# 创建QRadioButton对象btn1，设置内容"男"
btn1 = QRadioButton("男")
# 创建QRadioButton对象btn2，设置内容"女"
btn2 = QRadioButton("女")
# 设置btn2为选中状态
btn2.setChecked(True)
# 创建QButtonGroup对象group，将btn1和btn2添加到group
group = QButtonGroup()
group.addButton(btn1)
group.addButton(btn2)
# group的信号buttonToggled连接槽函数on_group_toggled
group.buttonToggled.connect(on_group_toggled)

# 创建VBoxLayout对象layout，将btn1和btn2添加到layout
layout = QVBoxLayout()
layout.addWidget(btn1)
layout.addWidget(btn2)
# 窗口添加布局
w.setLayout(layout)

# 3.显示窗口
w.show()

# 4.等待窗口停止
sys.exit(app.exec_())
