import sys
from PyQt5.QtWidgets import *
from ui.Ui_main_window import Ui_MainWindow
from views.serial_assist_widget import SerialAssistWidget
from views.bluetooth_assist_widget import BleAssistWidget
from views.ble_control_widget import BleControlWidget


class MainWindow(QMainWindow):
    """注意：父类必须是QMainWindow"""
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        # 设置窗口大小
        self.resize(850, 600)

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        # QTabWidget的添加widget
        self.ui.tabWidget.addTab(SerialAssistWidget(self), "串口助手")
        self.ui.tabWidget.addTab(BleAssistWidget(self), "蓝牙助手")
        self.ui.tabWidget.addTab(BleControlWidget(self), "小车控制器")
        
        # 显示指定的tab
        self.ui.tabWidget.setCurrentIndex(2)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    W = MainWindow()
    W.show()

    sys.exit(app.exec_())