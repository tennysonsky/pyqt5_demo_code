#include    "RTX51TNY.H"

#include    "UART.h"
#include    "NVIC.h"
#include    "Switch.h"
#include    "GPIO.h"
#include	"STC8H_PWM.h"

#include    "Light.h"
#include    "Key.h"
#include    "Battery.h"
#include    "Buzzer.h"
#include    "Ultrasonic.h"
#include    "Track.h"
#include    "Motors.h"

// 全局变量
float distance;    // 超声波测的距离


void GPIO_config() {
    //准双向口	uart1: P30 P31
    P3_MODE_IO_PU(GPIO_Pin_0 | GPIO_Pin_1);

    //准双向口	uart2: P10 P11
    P1_MODE_IO_PU(GPIO_Pin_0 | GPIO_Pin_1);
    
    // 蓝牙 en引脚 P03
    P0_MODE_IO_PU(GPIO_Pin_3);
}

void UART_config(void) {
    // >>> 记得添加 NVIC.c, UART.c, UART_Isr.c <<<
    COMx_InitDefine		COMx_InitStructure;					//结构定义
    // uart1
    COMx_InitStructure.UART_Mode      = UART_8bit_BRTx;	//模式, UART_ShiftRight,UART_8bit_BRTx,UART_9bit,UART_9bit_BRTx
    COMx_InitStructure.UART_BRT_Use   = BRT_Timer1;			//选择波特率发生器, BRT_Timer1, BRT_Timer2 (注意: 串口2固定使用BRT_Timer2)
    COMx_InitStructure.UART_BaudRate  = 115200ul;			//波特率, 一般 110 ~ 115200
    COMx_InitStructure.UART_RxEnable  = ENABLE;				//接收允许,   ENABLE或DISABLE
    COMx_InitStructure.BaudRateDouble = DISABLE;			//波特率加倍, ENABLE或DISABLE
    UART_Configuration(UART1, &COMx_InitStructure);		//初始化串口1 UART1,UART2,UART3,UART4

    NVIC_UART1_Init(ENABLE,Priority_1);		//中断使能, ENABLE/DISABLE; 优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3
    UART1_SW(UART1_SW_P30_P31);		// 引脚选择, UART1_SW_P30_P31,UART1_SW_P36_P37,UART1_SW_P16_P17,UART1_SW_P43_P44

    // uart2
    COMx_InitStructure.UART_Mode      = UART_8bit_BRTx;	//模式, UART_ShiftRight,UART_8bit_BRTx,UART_9bit,UART_9bit_BRTx
    COMx_InitStructure.UART_BRT_Use   = BRT_Timer2;			//选择波特率发生器, BRT_Timer1, BRT_Timer2 (注意: 串口2固定使用BRT_Timer2)
    COMx_InitStructure.UART_BaudRate  = 9600ul;			//波特率, 一般 110 ~ 115200
    COMx_InitStructure.UART_RxEnable  = ENABLE;				//接收允许,   ENABLE或DISABLE
    COMx_InitStructure.BaudRateDouble = DISABLE;			//波特率加倍, ENABLE或DISABLE
    UART_Configuration(UART2, &COMx_InitStructure);		//初始化串口1 UART1,UART2,UART3,UART4

    NVIC_UART2_Init(ENABLE,Priority_1);		//中断使能, ENABLE/DISABLE; 优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3
    UART2_SW(UART2_SW_P10_P11);		// 引脚选择
}


void sys_init() {
    EAXSFR();		/* 扩展寄存器访问使能 */

    UART_config();
    GPIO_config();

    // 驱动初始化
    Light_init();
    Key_init();
    Battery_init();
    Buzzer_init();
    Ultrasonic_init();
    Track_init();
    Motors_init();

    EA = 1;
}

// 程序入口
void start() _task_ 0 {
    sys_init();

    // 创建任务1 扫描按键
    os_create_task(1);

    // 不要创建任务2，用于巡线
    // 创建任务3、4，用于uart1, uart2接收数据
    os_create_task(3);
    os_create_task(4);
    
    // 创建任务5，超声波巡线
    os_create_task(5);

    // 删除任务0
    os_delete_task(0);

}

u8 flag = 0;
u8 track_flag = 0;   // 没有开启寻迹标志

// 按下的回调函数
void Key_on_keydown() {
    // printf("key down\n");
    switch (flag) {
    case 0:
        PrintString1("前进\n");
        Motors_forward(60);
        break;
    case 1:
        PrintString1("后退\n");
        Motors_backward(60);
        break;
    case 2:
        PrintString1("车灯亮灭三次\n");
        Light_on(ALL);
        os_wait2(K_TMO, 40); 
        Light_off(ALL);
        os_wait2(K_TMO, 40);  
        Light_on(ALL);
        os_wait2(K_TMO, 40); 
        Light_off(ALL);
        os_wait2(K_TMO, 40); 
        Light_on(ALL);
        os_wait2(K_TMO, 40); 
        Light_off(ALL);
        os_wait2(K_TMO, 40); 
        Buzzer_beep();
        break;
    case 3:
        PrintString1("启动巡线模式\n");
        os_create_task(2);
        track_flag = 1;   // 开启寻迹标志
        
        break;
    case 4:
        PrintString1("关闭巡线模式\n");
        if (track_flag == 1) {
            os_delete_task(2);
            Motors_stop();
            track_flag = 0;   // 没有开启寻迹标志
        }
        
        break;
    }

    flag ++;

    if (flag > 4) {
        flag = 0;
    }
    
    //P03 = 1;

}

// 抬起的回调函数
void Key_on_keyup() {
    // printf("key up\n");
    //P03 = 0;
    if (flag == 1 || flag == 2)  {
        Motors_stop();
    }
}

void do_work(u8 dat) {
    if(dat == 0x00) {
        // stop
        Motors_stop();
    } else if(dat == 0x01) {
        // forward
        Motors_forward(70);
    }  else if(dat == 0x02) {
        // backward
        Motors_backward(70);
    }  else if(dat == 0x03) {
        // left
        Motors_left(70);
    }  else if(dat == 0x04) {
        // right
        Motors_right(70);
    }  else if(dat == 0x05) {
        // around
        Motors_around(70);
    }  else if(dat == 0x10) {
        // buzzer open
        Buzzer_beep();
        Buzzer_beep();
    }  else if(dat == 0x11) {
        // 测距，给蓝牙发距离
        // 距离不在 2~400cm 范围，距离值没有意义
        // 打印已经配置uart2，给蓝牙发送数据
        printf("[distance]%.2f", distance);
    }  else if(dat == 0x12) {
        // light open
        Light_on(ALL);
        Buzzer_beep();        
    }  else if(dat == 0x13) {
        // light close
        Light_off(ALL);
        // Buzzer_beep(); 
    }  else if(dat == 0x20) {
        // start track
        os_create_task(2);
        track_flag = 1;   // 开启寻迹标志
    }  else if(dat == 0x21) {
        // stop track
        if (track_flag == 1) {
            os_delete_task(2);
            Motors_stop();
            track_flag = 0;   // 没有开启寻迹标志
        }       
    }
}


void task_1() _task_ 1 {

    while (1) {
        // 扫描按键
        Key_scan();

        os_wait2(K_TMO, 20);  // 5ms * 20
    }
}

// 检查寻迹
void task_2() _task_ 2 {
//    u8 states[5];
//    u8 i;
    int pos, last_pos = -10000;

    while (1) {
//        Track_get_state(states);
//        for (i = 0; i < 5; i++) {
//            printf("%d ", (int)states[i]);
//        }
//        printf("\n");
//
        pos = Track_get_position();
        // 上次的pos和本次pos相同, 跳过本次循环, 避免频繁设置同样PWM
        if(last_pos == pos){
                os_wait2(K_TMO, 2); // 5ms * 2 = 10ms
                continue;
        }
        
        last_pos = pos;
         
        // printf("pos = %d\n", pos);
        // 如果pos == 0， 前进
        if (pos == 0) {
            Motors_forward(70);
        } else if (pos < 0) {
            // 少于0，左转
            Motors_left(70);
        }  else if (pos > 0) {
            // 大于0，右转
            Motors_right(70);
        }

        os_wait2(K_TMO, 4);  // 20ms
    }
}

// uart1收到数据，通过uart2发出去
void task_3() _task_ 3 {
    u8 i;
    while (1) {
        if(COM1.RX_TimeOut > 0) {
            //超时计数
            if(--COM1.RX_TimeOut == 0) {
                if(COM1.RX_Cnt > 0) {
                    for(i=0; i<COM1.RX_Cnt; i++)	{
                        // RX1_Buffer[i]存的是接收的数据，通过uart2发送 TX2_write2buff
                        // RX1_Buffer[i] 就是uart1收到的数据
                        TX2_write2buff(RX1_Buffer[i]);
                    }
                }
                COM1.RX_Cnt = 0;
            }
        }

        os_wait2(K_TMO, 4);  // 5ms * 4
    }
}


// uart2收到数据，通过uart1发出去
void task_4() _task_ 4 {
    u8 i;
    while (1) {
        if(COM2.RX_TimeOut > 0) {
            //超时计数
            if(--COM2.RX_TimeOut == 0) {
                if(COM2.RX_Cnt > 0) {
                    for(i=0; i<COM2.RX_Cnt; i++)	{
                        
                        do_work(RX2_Buffer[i]);
                        
                        // RX2_Buffer[i]存的是接收的数据，通过uart1发送用 TX1_write2buff
                        // RX2_Buffer[i]就是uart2收到的数据
                        TX1_write2buff(RX2_Buffer[i]);
                    }
                }
                COM2.RX_Cnt = 0;
            }
        }

        os_wait2(K_TMO, 4);  // 5ms * 4
    }
}

// 超声波巡线
void task_5() _task_ 5 { 
     char res;
     while (1) {
        res = Ultrasonic_get_distance(&distance);
        if (res == 0) {
            // printf("距离为：%.2f cm\n", distance);
            // 距离低于30cm，无源蜂鸣器响起，  停止寻迹
            if (distance < 30.0) {
                // printf("[waring]distance < 30.0 cm");
                Buzzer_beep();
                if (track_flag == 1) {
                    os_delete_task(2);
                    Motors_stop();
                    track_flag = 0;   // 没有开启寻迹标志
                }   
            }
            
        } else {
            // printf("res = %d\n", (int)res);
        }
        
        os_wait2(K_TMO, 20);  // 5ms * 20
     }
}