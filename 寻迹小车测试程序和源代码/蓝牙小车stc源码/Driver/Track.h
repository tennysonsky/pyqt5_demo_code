#ifndef __TRACK_H__
#define __TRACK_H__

#include "Config.h"

#define TRACK_0		P33  // ��1
#define TRACK_1		P32  // ��2
#define TRACK_2		P50  // ��
#define TRACK_3		P06  // ��1
#define TRACK_4		P47  // ��2

void Track_init();

void Track_get_state(u8 states[]);

int  Track_get_position();

#endif