#include "Key.h"

// 初始化
void Key_init() {
    P3_MODE_IO_PU(GPIO_Pin_7);
}

#define UP      1
#define DOWN    0

u8 last_state = UP; // 默认状态为抬起

// 扫描按键
void Key_scan() {
    // 如果当前的按下，刚刚是抬起
    if (KEY == DOWN && last_state == UP) {
        //printf("key down\n");
        Key_on_keydown();
        
        last_state = DOWN;
    } else if (KEY == UP && last_state == DOWN) {
        // 如果当前的抬起，刚刚是按下
        // printf("key up\n");
        Key_on_keyup();
        last_state = UP;
    }

}