#include "Light.h"

// 初始化
void Light_init() {
    // IO配置准双向口，不够亮
//    P0_MODE_IO_PU(GPIO_Pin_5);
//    P3_MODE_IO_PU(GPIO_Pin_4);
    
    // 推挽
    P0_MODE_OUT_PP(GPIO_Pin_5);
    P3_MODE_OUT_PP(GPIO_Pin_4);

    // 灯全灭
    Light_off(ALL);

}

// 开灯
void Light_on(Light light) {
    if (light == LEFT) {
        LED_LEFT = 1;
    } else if (light == RIGHT) {
        LED_RIGHT = 1;
    }  else if (light == ALL) {
        LED_LEFT = 1;
        LED_RIGHT = 1;
    }
}


// 关灯
void Light_off(Light light) {
    if (light == LEFT) {
        LED_LEFT = 0;
    } else if (light == RIGHT) {
        LED_RIGHT = 0;
    }  else if (light == ALL) {
        LED_LEFT = 0;
        LED_RIGHT = 0;
    }
}