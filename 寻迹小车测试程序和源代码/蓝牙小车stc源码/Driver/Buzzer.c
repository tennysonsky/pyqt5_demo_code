#include "Buzzer.h"
#include "GPIO.h"

void Buzzer_init() {
     P3_MODE_OUT_PP(GPIO_Pin_6);
}

static void Delay100us(void)	//@24.000MHz
{
	unsigned char data i, j;

	i = 4;
	j = 27;
	do
	{
		while (--j);
	} while (--i);
}



// 无源蜂鸣器
void Buzzer_beep() { 
    // 1次循环一次高电平  1次循环一次低电平  每一次0.1ms
    // 一个周期 0.2ms， 2次循环才能形成1个周期
    // 频率，每秒钟执行的1个周期次数  1000ms / 0.2ms = 5000 hz
    int i;   // 别用u8
    BUZZER_PIN = 1;
    for (i = 0; i < 3000; i++) {
        BUZZER_PIN = !BUZZER_PIN;
        Delay100us();
    }
    BUZZER_PIN = 0;
    

}