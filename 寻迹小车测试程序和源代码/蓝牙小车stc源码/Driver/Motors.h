#ifndef ___MOTORS_H__
#define ___MOTORS_H__

#include "config.h"

void Motors_init();

// 前进 0~100
void Motors_forward(int speed);
// 后退 0~100
void Motors_backward(int speed);
// 左转 0~100
void Motors_left(int speed);
// 右转 0~100
void Motors_right(int speed);
// 原地旋转 0~100
void Motors_around(int speed);

// 停止
void Motors_stop();

#endif