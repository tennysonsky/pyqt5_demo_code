#include "Track.h"
#include "GPIO.h"

static void GPIO_config() {
     // 准双向
    GPIO_InitTypeDef	GPIO_InitStructure;		//结构定义
    GPIO_InitStructure.Pin  = GPIO_Pin_2 | GPIO_Pin_3;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_PullUp;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P3, &GPIO_InitStructure);
    
    GPIO_InitStructure.Pin  = GPIO_Pin_0;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_PullUp;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P5, &GPIO_InitStructure);
    
    GPIO_InitStructure.Pin  = GPIO_Pin_6;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_PullUp;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P0, &GPIO_InitStructure);
    
    GPIO_InitStructure.Pin  = GPIO_Pin_7;		//指定要初始化的IO,
    GPIO_InitStructure.Mode = GPIO_PullUp;	//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
    GPIO_Inilize(GPIO_P4, &GPIO_InitStructure);

}

void Track_init() {
    GPIO_config();
}

/*
- 高电平-不亮-压到线
- 低电平-亮起-正常反射面
*/
void Track_get_state(u8 states[]) {
    states[0] = TRACK_0;
    states[1] = TRACK_1;
    states[2] = TRACK_2;
    states[3] = TRACK_3;
    states[4] = TRACK_4;
}

/*
- 高电平-不亮-压到线
- 低电平-亮起-正常反射面
*/

int last_pos = 0; // 上一次的状态
int  Track_get_position() {
    // 压到线坐标值的累加
    u8 cnt = 0;
    int sum = 0, pos;
    
    if (TRACK_0 == 1) {
        sum +=  -64;
        cnt ++;
    }
    
    if (TRACK_1 == 1) {
        sum +=  -32;
        cnt ++;
    }
    
    if (TRACK_2 == 1) {
        sum +=  0;
        cnt ++;
    }
    
    if (TRACK_3 == 1) {
        sum +=  32;
        cnt ++;
    }
     
    if (TRACK_4 == 1) {
        sum +=  64;
        cnt ++;
    }
    
    // 没有压到线，返回上一次的状态
    if (cnt == 0) {
        return last_pos;
    }
    
    pos = sum / cnt;
    
    //  执行到这，说明压到线了，把压到线的最后状态保存一下
    // 留给下一次没有压到线作为返回值更新
    last_pos = pos;

    return pos;
}


