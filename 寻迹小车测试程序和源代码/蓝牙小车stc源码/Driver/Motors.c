#include    "Motors.h"
#include    "NVIC.h"
#include    "Switch.h"
#include    "GPIO.h"
#include	"STC8H_PWM.h"

typedef struct {
    int LQ_speed;
    int LH_speed;
    int RQ_speed;
    int RH_speed;
} MotorCfg;

static void GPIO_config() {
    P2_MODE_IO_PU(GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3);
    P1_MODE_IO_PU(GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7);
}

// -100 --------- 0 --------- 100		速度
//后退最大速度	  0			前进最大速度
// 0 ----------  50 --------- 100		PWM占空比
int speed2duty(int speed) { // 参数范围：-100 --------- 0 --------- 100		速度
    // speed > 0 前进
    // speed < 0 后退

    // [-100, 100]
    // speed / 2  	    =>  [-50,  50]
    // speed / 2 + 50   =>  [0, 100]
    return speed / 2 + 50;
}

#define PERIOD (MAIN_Fosc / 1000)
void	Motors_cfg(MotorCfg cfg)  // 如果是0，说明停止
{
    PWMx_InitDefine		PWMx_InitStructure;
    
    // 配置PWM1  左后轮
	PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
	PWMx_InitStructure.PWM_Duty    		= speed2duty(cfg.LH_speed) * PERIOD / 100;	//PWM占空比时间, 0~Period
	PWMx_InitStructure.PWM_EnoSelect    = (cfg.LH_speed == 0 ? 0 : (ENO1P | ENO1N));	//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
	PWM_Configuration(PWM1, &PWMx_InitStructure);	
    
    // 配置PWM2  右后轮        模式2
	PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE2;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
	PWMx_InitStructure.PWM_Duty    		= speed2duty(cfg.RH_speed) * PERIOD / 100;	//PWM占空比时间, 0~Period
	PWMx_InitStructure.PWM_EnoSelect    = (cfg.RH_speed == 0 ? 0 : (ENO2P | ENO2N));	//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
	PWM_Configuration(PWM2, &PWMx_InitStructure);	
    
    // 配置PWM3  右前轮
	PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE1;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
	PWMx_InitStructure.PWM_Duty    		= speed2duty(cfg.RQ_speed) * PERIOD / 100;	//PWM占空比时间, 0~Period
	PWMx_InitStructure.PWM_EnoSelect    = (cfg.RQ_speed == 0 ? 0 : (ENO3P | ENO3N));	//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
	PWM_Configuration(PWM3, &PWMx_InitStructure);	
		
	// 配置PWM4  左前轮         模式2
	PWMx_InitStructure.PWM_Mode    		= CCMRn_PWM_MODE2;	//模式,		CCMRn_FREEZE,CCMRn_MATCH_VALID,CCMRn_MATCH_INVALID,CCMRn_ROLLOVER,CCMRn_FORCE_INVALID,CCMRn_FORCE_VALID,CCMRn_PWM_MODE1,CCMRn_PWM_MODE2
	PWMx_InitStructure.PWM_Duty    		= speed2duty(cfg.LQ_speed) * PERIOD / 100;	//PWM占空比时间, 0~Period
	PWMx_InitStructure.PWM_EnoSelect    = (cfg.LQ_speed == 0 ? 0 : (ENO4P | ENO4N));	//输出通道选择,	ENO1P,ENO1N,ENO2P,ENO2N,ENO3P,ENO3N,ENO4P,ENO4N / ENO5P,ENO6P,ENO7P,ENO8P
	PWM_Configuration(PWM4, &PWMx_InitStructure);	
	
	// 配置PWMA
    PWMx_InitStructure.PWM_Period   = PERIOD - 1;			//周期时间,   0~65535
    PWMx_InitStructure.PWM_DeadTime = 0;					//死区发生器设置, 0~255
    PWMx_InitStructure.PWM_MainOutEnable= ENABLE;			//主输出使能, ENABLE,DISABLE
    PWMx_InitStructure.PWM_CEN_Enable   = ENABLE;			//使能计数器, ENABLE,DISABLE
    PWM_Configuration(PWMA, &PWMx_InitStructure);			//初始化PWM通用寄存器,  PWMA,PWMB

	// 切换PWM通道
    // 左后轮
	PWM1_SW(PWM1_SW_P20_P21);
    // 右后轮
	PWM2_SW(PWM2_SW_P22_P23);
    // 右前轮
	PWM3_SW(PWM3_SW_P14_P15);
    // 左前轮
	PWM4_SW(PWM4_SW_P16_P17);			//PWM3_SW_P14_P15,PWM3_SW_P24_P25,PWM3_SW_P64_P65


	// 初始化PWMA的中断
    NVIC_PWM_Init(PWMA,DISABLE,Priority_0);
}

void Motors_init() {
    EAXSFR();		/* 扩展寄存器访问使能 */
    
    GPIO_config();
}

// 前进
void Motors_forward(int speed) {
    MotorCfg cfg = {0};
    cfg.LQ_speed = speed;
    cfg.LH_speed = speed;
    cfg.RQ_speed = speed;
    cfg.RH_speed = speed;
    Motors_cfg(cfg);
}

// 后退
void Motors_backward(int speed) {
    MotorCfg cfg = {0};
    cfg.LQ_speed = -speed;
    cfg.LH_speed = -speed;
    cfg.RQ_speed = -speed;
    cfg.RH_speed = -speed;
    Motors_cfg(cfg);
}

// 左转 右侧轮子的转速大于左侧即可
void Motors_left(int speed) {
    MotorCfg cfg = {0};
    cfg.RQ_speed = speed;
    cfg.RH_speed = speed;
    Motors_cfg(cfg);
}

// 右转 左侧轮子的转速大于右侧即可
void Motors_right(int speed) {
    MotorCfg cfg = {0};
    cfg.LQ_speed = speed;
    cfg.LH_speed = speed;
    Motors_cfg(cfg);
}

// 原地旋转 0~100
void Motors_around(int speed) {
    MotorCfg cfg = {0};
    cfg.LQ_speed = speed;
    cfg.LH_speed = speed;
    cfg.RQ_speed = -speed;
    cfg.RH_speed = -speed;
    Motors_cfg(cfg);
}

// 停止
void Motors_stop() {
     MotorCfg cfg = {0};  // 初始化所有成员为0
     Motors_cfg(cfg);
}