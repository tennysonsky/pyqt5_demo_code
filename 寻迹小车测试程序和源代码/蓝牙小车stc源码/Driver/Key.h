#ifndef __KEY_H__
#define __KEY_H__

#include "GPIO.h"

#define KEY		P37

// 初始化
void Key_init();
// 扫描按键
void Key_scan();
//// 按下抬起回调函数的声明，需用户在合适位置定义
extern void Key_on_keydown();
extern void Key_on_keyup();

#endif