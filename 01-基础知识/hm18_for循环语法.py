"""
for循环的主要作用是遍历数据（容器)）中的元素
语法：
for 临时变量 in 容器变量:
    执行的代码

说明：in 操作符用于判断元素是否存在于容器中，如果在容器中,返回 True，否则返回 False

需求：打印字符串"hello world"中每一个字符
"""
print("=" * 50)
data = "hello world"
# temp为临时变量，合法即可
for temp in data:
    print(temp)

print("=" * 50)
# 元素在容器中,返回 True
print("h" in "hello")
# 元素不在容器中,返回 False
print("h1" in "hello")