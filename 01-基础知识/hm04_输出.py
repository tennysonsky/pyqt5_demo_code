"""
基本输出：
    print(数据1, 数据2 ……)
格式化输出：
    # 右边的数据按顺序依次放在左边的{}中
    print('{},{}'.format(数据1，数据2))
    # 直接输出{}里面的数据，f也可以写成大写的F，Python3.6及以上解释器才支持
    print(f'{数据1},{数据2}')

需求：
# 1. 定义3个变量，分别保存姓名'mike'，年龄35，城市'sz'
# 2. 格式化输出：姓名：mike，年龄：35，城市：sz
"""

print('=' * 50)
# 1. 直接输出内容
print('mike', 18, 'sz')

# 2. 先定义变量，再输出变量的内容
name = 'mike'
age = 35
city = 'sz'
print(name, age, city)

print('=' * 50)
# 1. 定义3个变量，分别保存姓名'mike'，年龄35，城市'sz'
name = 'mike'
age = 35
city = 'sz'
# 2. 格式化输出：姓名：mike，年龄：35，城市：sz
# 右边format里面name, age, city的内容分别放在第1个{}, 第2个{}, 第3个{}的位置
print('姓名：{}，年龄：{}，城市：{}'.format(name, age, city))

print('=' * 50)
# 1. 定义3个变量，分别保存姓名'mike'，年龄35，城市'sz'
name = 'mike'
age = 35
city = 'sz'
# 2. 格式化输出：姓名：mike，年龄：35，城市：sz
print(f'姓名：{name}，年龄：{age}，城市：{city}')

# 小数点保留2位输出
print(f'小数点保留2位输出：{3.1415926:.2f}')
