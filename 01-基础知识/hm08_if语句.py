"""
if语句语法：
if 判断的条件：
    条件成立时执行的代码

注意点：
1. 可以把整个 if 语句看成一个完整的代码块
2. 代码的缩进为一个 tab 键， 或者 4 个空格

案例需求:
# 1. 定义一个整数变量记录年龄
# 2. 判断是否满 18 岁 （ >=）
# 3. 如果满 18 岁， 允许进网吧嗨皮
"""

# 1. 定义一个整数变量记录年龄
age = 19
# 2. 判断是否满 18 岁 （ >=）
if age >= 18:
    # 3. 如果满 18 岁， 允许进网吧嗨皮
    print('允许进网吧嗨皮')

# 思考？
# print("这句代码什么时候执行?")
