print("=" * 50)
i = 1
while i < 10:    # 外循环控制几行
    j = 1
    while j <= i:    # 内循环控制一行几个*
        print(f"{j} * {i} = {j * i}\t", end="")
        j += 1
    print("")

    i += 1

print("=" * 50)
i = 9
while i > 0:    # 外循环控制几行
    j = 1
    while j <= i:    # 内循环控制一行几个*
        print(f"{j} * {i} = {j * i}\t", end="")
        j += 1
    print("")

    i -= 1
