"""
打印出所有"水仙花数"，所谓"水仙花数"是指一个三位数[100, 1000)，其各位数字立方和等于该数本身。
例如：153是一个"水仙花数"，因为153=1的三次方＋5的三次方＋3的三次方。

分析：
1. 遍历所有的三位数
2. 求 百位 十位个位  立方和
3. 判断并打印
"""

for ele in range(100, 1000):
    # 百位
    hun = ele // 100
    # 十位
    ten = ele // 10 % 10
    # 个位
    single = ele % 10
    # 判断立方和
    if (hun * hun * hun + ten * ten * ten + single * single * single) == ele:
        print(ele)
