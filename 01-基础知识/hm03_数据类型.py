"""
250             int, 整型
3.14            float 浮点型
'mike'          str 字符串，只要是''格式的内容就是字符串
"mike"          str 字符串，只要是""格式的内容就是字符串
True, False     bool, 布尔, 非0就是真
None            NoneType, 空类型
[1, 3, 5]       list, 列表 [元素1, 元素2, 元素3]
(1, 3, 5)       tuple, 元组 (元素1, 元素2, 元素3)

# 查看数据类型类型
# 语法: type(数据/变量名)
# 注意: type() 函数需要配合 print() 输出函数使用
"""

# 定义变量，无需指定类型，系统会自动推导，自动根据赋值的内容推导这个变量的类型
name = 'mike'  # 字符串
age = 20  # 整型
height = 1.78  # 浮点型
is_man = True  # 布尔类型
temp = None  # 空类型
list_data = [1, 3, 5]
tuple_data = (1, 3, 5)

# 查看数据类型类型
# 语法: type(数据/变量名)
# 注意: type() 函数需要配合 print() 输出函数使用
print(type(name))
print(type(age))
print(type(height))
print(type(is_man))
print(type(temp))
print(type(list_data))
print(type(tuple_data))
