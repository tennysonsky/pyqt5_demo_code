"""
计算器练习需求：
需求:
● 用户输入整型变量a
● 用户输入整形变量b
● 计算输出a+b=?
"""
print("=" * 50)
a = int(input("请输入整型内容："))
b = int(input("请输入整型内容："))
print(f"{a} + {b} = {a + b}")

"""
超市买苹果计算金额
需求:	
●  收银员输入苹果的价格price，单位：元/斤
●  收银员输入用户购买苹果的重量weight，单位：斤
●   计算并输出付款金额:xxx元
"""
print("=" * 50)
price = float(input("输入苹果的价格(元/斤):"))
weight = float(input("输入苹果的重量(单位：斤):"))
print(f"付款金额: {price * weight}元")

"""
在控制台依次提示用户输入：姓名name、公司com、职位title、电话telephone、邮箱email
"""
print("=" * 50)
name = input("请输入姓名：")
com = input("请输入公司：")
title = input("请输入职位：")
telephone = input("请输入电话：")
email = input("请输入邮箱：")

print("*" * 50)
print(com)
print("")
print(f"{name}({title})")
print("")
print(f"电话：{telephone}")
print(f"邮箱：{email}")
print("*" * 50)

