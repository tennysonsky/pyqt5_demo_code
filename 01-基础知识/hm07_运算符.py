"""
算术运算符注意点：
# 字符串之间可以相加
# 字符串可和整型相乘
# 字符串不可和整型相加
"""
# 字符串之间可以相加
print("hello" + "abc")
# 字符串可和整型相乘，打印5个"hello"
print("hello" * 5)
# # 字符串不可和整型相加
# print("hello" + 5)  # err

# 非0就是True
print(True and True)  # True
print((5 > 4) and (4 < 7))  # True
print(True or False)  # True
print(not (True or False))  # False
