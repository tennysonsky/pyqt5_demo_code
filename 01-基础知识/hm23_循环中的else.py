"""
while或for循环:
    循环体代码
else:
    没有通过break退出循环, 循环结束后, 会执行的代码
"""

for i in range(1, 6):
    if i == 3:
        print('吃饱了，不吃了')
        # break  # 结束循环，退出循环

    print(f'吃第{i}碗🍚')
else:
    print("没有通过break退出循环, 循环结束后, 会执行的代码")