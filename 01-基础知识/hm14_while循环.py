"""
# 循环：重复执行某段代码

# 1. 定义一个条件变量，也叫计数器(条件定义)
# 2. while 循环条件：(条件控制)
    # 2.1 满足条件的代码块
    # 2.2 条件变量的改变【非常重要】(条件改变)

# 需求：跑步10圈
"""

# 1. 定义一个条件变量，也叫计数器(条件定义)
i = 0  # Python计数器一般从0开始

# 2. while 循环条件：(条件控制)
while i < 10:
    # 2.1 满足条件的代码块
    print(f'跑步第{i}圈')

    # 2.2 条件变量的改变【非常重要】(条件改变)
    i += 1  # i = i + 1

# 循环结束后， 之前定义的计数器条件的数值是依旧存在的
print(f"循环结束后的 i = {i}")