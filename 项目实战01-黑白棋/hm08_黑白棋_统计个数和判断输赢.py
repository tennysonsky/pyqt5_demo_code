import sys

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from ui.Ui_main_widget import Ui_MainWidget
from utils import *


class MyWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_MainWidget()
        self.ui.setupUi(self)
        # 设置窗口最小大小
        self.setMinimumSize(900, 800)

        # 实例属性
        self.grid_width = 0  # 格子宽度
        self.grid_height = 0  # 格子高度
        self.start_x = 0  # 棋盘起点x坐标
        self.start_y = 0  # 棋盘起点y坐标
        self.press_i = -1 # 落子的下标
        self.press_j = -1
        
        self.current_role = BLACK # 当前落子角色
        
        self.leftTimer = QTimer() # 实例化定时器
        self.num = 15    # 计数器
        self.ui.lcdTimer.display(self.num)
        self.leftTimer.start(1000) # 启动定时器
        
        # 定时器信号和槽连接
        self.leftTimer.timeout.connect(self.timerSlot)

        # 创建一个8x8的二维数组
        # EMPTY 0表示没有棋子，BLACK 1表示黑棋，WHITE 2表示白棋
        self.chess = [[EMPTY for _ in range(8)] for _ in range(8)]
        # 设置棋盘中间4个棋子状态
        self.chess[3][3] = BLACK
        self.chess[3][4] = WHITE
        self.chess[4][3] = WHITE
        self.chess[4][4] = BLACK

        # print(self.chess)

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        # 隐藏白
        self.ui.labelWhite.hide()
    
    def timerSlot(self):
        # 计数器-1
        self.num -= 1
        # 数码管显示
        self.ui.lcdTimer.display(self.num)
        if self.num == 0:
            # 改变角色
            self.switch_role()
    
    def switch_role(self):
        self.num = 15    # 计数器
        self.ui.lcdTimer.display(self.num)
        
        """切换下子角色"""
        if self.current_role == BLACK:
            self.current_role = WHITE
            # 显示黑，隐藏白
            self.ui.labelBlack.hide()
            self.ui.labelWhite.show()
        else:
            self.current_role = BLACK
            # 显示白，隐藏黑
            self.ui.labelBlack.show()
            self.ui.labelWhite.hide()
            
        # 判断输赢
        self.judge_result()
        
    def judge_result(self):
        """判断输赢：双方都不能吃子即可判断输赢"""
        black_count = 0
        white_count = 0
        is_over = True # 结束标志位，默认为True，代表结束
        
        for i in range(8):
            for j in range(8):
                if self.chess[i][j] == BLACK:
                    black_count += 1
                elif self.chess[i][j] == WHITE:
                    white_count += 1
                    
                # 判断黑白子在i, j位置，能否吃子，没有改变chess的数组
                # > 0，说明能吃子，游戏还没结束
                if judge_rule(i, j, self.chess, BLACK, False) > 0 \
                    or judge_rule(i, j, self.chess, WHITE, False) > 0:
                    is_over = False # 游戏还没结束
                    
        # lcd显示黑白棋个数
        self.ui.lcdBlack.display(black_count)
        self.ui.lcdWhite.display(white_count)
                    
        if is_over == False: # 游戏还没结束，不往下执行
            return
        
        # 如果定时器启动，关闭定时器
        if self.leftTimer.isActive():
            self.leftTimer.stop()
            
        self.ui.labelBlack.show()
        self.ui.labelWhite.show()
        
        # 判断输赢，弹出对话框显示谁赢
        if black_count > white_count:
            QMessageBox.information(self, '黑棋胜利', '黑棋胜利')
        elif black_count < white_count:
            QMessageBox.information(self, '白棋胜利', '白棋胜利')
        else:
            QMessageBox.information(self, '平局', '平局')
        
        

    def paintEvent(self, event: QPaintEvent):  # 绘图事件
        # 调用父类同名方法
        super().paintEvent(event)

        # 定义画家（请了一个画家来画画），画在主窗口上
        painter = QPainter(self)

        # 以窗口大小画背景图
        painter.drawPixmap(
            0, 0, self.width(), self.height(), QPixmap("./img/board.jpg")
        )

        # 窗口宽度分12份
        self.grid_width = self.width() // 12
        # 窗口高度分12份
        self.grid_height = self.height() // 12

        # 棋盘起点坐标为grid_width， grid_height
        self.start_x = self.grid_width * 2
        self.start_y = self.grid_height * 2

        # 设置线宽为2
        line_width = 4
        painter.setPen(QPen(Qt.black, line_width))

        # 取中间8份画棋盘
        for i in range(9):
            # 画横线
            painter.drawLine(
                self.start_x,
                self.start_y + self.grid_height * i,
                self.start_x + self.grid_width * 8,
                self.start_y + self.grid_height * i,
            )
            # 画竖线
            painter.drawLine(
                self.start_x + self.grid_width * i,
                self.start_y,
                self.start_x + self.grid_width * i,
                self.start_y + self.grid_height * 8,
            )

        # 画棋子
        for i in range(8):
            for j in range(8):
                if self.chess[i][j] == BLACK:
                    # 画黑棋
                    painter.drawPixmap(
                        self.start_x + self.grid_width * i,
                        self.start_y + self.grid_height * j,
                        self.grid_width - line_width,
                        self.grid_height - line_width,
                        QPixmap("./img/black.png"),
                    )
                elif self.chess[i][j] == WHITE:
                    # 画白棋
                    painter.drawPixmap(
                        self.start_x + self.grid_width * i,
                        self.start_y + self.grid_height * j,
                        self.grid_width - line_width,
                        self.grid_height - line_width,
                        QPixmap("./img/white.png"),
                    )
                    
        if self.press_i != -1:
            # 设置画笔颜色为绿色
            painter.setPen(QPen(Qt.green, line_width))
            # 画矩形
            painter.drawRect(
                self.start_x + self.grid_width * self.press_i,
                self.start_y + self.grid_height * self.press_j,
                self.grid_width,
                self.grid_height,
            )
    def mousePressEvent(self, event: QMouseEvent):  # 鼠标按下事件
        # 调用父类同名方法
        super().mousePressEvent(event)
        # 获取 x, y 坐标
        x = event.x()
        y = event.y()

        # 范围判断，点击范围不要超过棋盘
        if (
            x >= self.start_x
            and x <= self.start_x + self.grid_width * 8
            and y >= self.start_y
            and y <= self.start_y + self.grid_height * 8
        ):
            self.press_i = (x - self.start_x) // self.grid_width
            self.press_j = (y - self.start_y) // self.grid_height
            print(self.press_i, self.press_j)
            
            # self.chess[self.press_i][self.press_j] = self.current_role
            # 吃子
            eat_num = judge_rule(self.press_i, self.press_j, self.chess, self.current_role)
            if eat_num > 0: # 成功吃子后再操作
                self.switch_role() # 切换下子角色
                self.update()  # 更新绘图区域

if __name__ == "__main__":
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.show()

    sys.exit(app.exec_())
