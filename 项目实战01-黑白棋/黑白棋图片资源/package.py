import PyInstaller.__main__

# 这里的是不需要打包进去的三方模块，可以减少软件包的体积，这里只是举个例子
excluded_modules = [
    "pytest",
    "selenium",
]

append_string = []
for mod in excluded_modules:
    append_string += [f'--exclude-module={mod}']

PyInstaller.__main__.run([
    '-y',  # 如果dist文件夹内已经存在生成文件，则不询问用户，直接覆盖
    # '-p', 'src', # 设置 Python 导入模块的路径（和设置 PYTHONPATH 环境变量的作用相似）。也可使用路径分隔符（Windows 使用分号;，Linux 使用冒号:）来分隔多个路径
    'hm11_黑白棋_图片资源.py',  # 主程序入口

    # '--onedir',  # -D 文件夹
    '--onefile', # -F 单文件

    # '--nowindowed', # -c 无窗口
    '--windowed',  # -w 有窗口

    '-n', '黑白棋游戏',
    '-i', 'img/chess.ico',
    '--add-data=img;img',  # 用法：pyinstaller main.py –add-data=src;dest。windows以;分割，mac,linux以:分割
    *append_string
])