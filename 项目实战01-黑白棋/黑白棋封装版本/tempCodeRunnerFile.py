for i in range(8):
            for j in range(8):
                if self.chess[i][j] == BLACK:
                    black_count += 1
                elif self.chess[i][j] == WHITE:
                    white_count += 1
                    
                # 判断黑白子在i, j位置，能否吃子，没有改变chess的数组
                # > 0，说明能吃子，游戏还没结束
                if judge_rule(i, j, self.chess, BLACK, False) > 0 \
                    or judge_r