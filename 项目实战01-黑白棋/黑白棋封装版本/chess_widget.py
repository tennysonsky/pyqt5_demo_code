from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from utils import *

"""
1. 自定义信号，类一定要继承于QObject，QWidget本身就继承于QObject
2. 通过pyqtSignal()方法来创建一个信号对象signal_mouse_press
3. 发送信号 signal_mouse_press.emit(参数列表)
"""

class ChessWidget(QWidget):
    # 通过pyqtSignal()方法来创建一个信号对象signal_mouse_press
    signal_mouse_press = pyqtSignal(int, int)
    
    def __init__(self, parent=None):
        super().__init__(parent)

        # 实例属性
        self.grid_width = 0  # 格子宽度
        self.grid_height = 0  # 格子高度
        self.start_x = 0  # 棋盘起点x坐标
        self.start_y = 0  # 棋盘起点y坐标
        self.press_i = -1 # 落子的下标
        self.press_j = -1

        # 创建一个8x8的二维数组，默认为None
        # EMPTY 0表示没有棋子，BLACK 1表示黑棋，WHITE 2表示白棋
        self.chess = None

    def paintEvent(self, event: QPaintEvent):  # 绘图事件
        # 调用父类同名方法
        super().paintEvent(event)

        # 定义画家（请了一个画家来画画），画在主窗口上
        painter = QPainter(self)

        # 以窗口大小画棋盘背景图
        painter.drawPixmap(
            0, 0, self.width(), self.height(), QPixmap("./img/board.jpg")
        )

        # 窗口宽度分10份
        self.grid_width = self.width() // 10
        # 窗口高度分10份
        self.grid_height = self.height() // 10

        # 棋盘起点坐标
        self.start_x = self.grid_width
        self.start_y = self.grid_height

        # 设置线宽为2
        line_width = 4
        painter.setPen(QPen(Qt.black, line_width))

        # 取中间8份画棋盘
        for i in range(9):
            # 画横线
            painter.drawLine(
                self.start_x,
                self.start_y + self.grid_height * i,
                self.start_x + self.grid_width * 8,
                self.start_y + self.grid_height * i,
            )
            # 画竖线
            painter.drawLine(
                self.start_x + self.grid_width * i,
                self.start_y,
                self.start_x + self.grid_width * i,
                self.start_y + self.grid_height * 8,
            )
            
        if self.chess is None: # 说明没有初始化棋盘
            return

        # 画棋子
        for i in range(8):
            for j in range(8):
                if self.chess[i][j] == BLACK:
                    # 画黑棋
                    painter.drawPixmap(
                        self.start_x + self.grid_width * i,
                        self.start_y + self.grid_height * j,
                        self.grid_width - line_width,
                        self.grid_height - line_width,
                        QPixmap("./img/black.png"),
                    )
                elif self.chess[i][j] == WHITE:
                    # 画白棋
                    painter.drawPixmap(
                        self.start_x + self.grid_width * i,
                        self.start_y + self.grid_height * j,
                        self.grid_width - line_width,
                        self.grid_height - line_width,
                        QPixmap("./img/white.png"),
                    )
                    
        if self.press_i != -1:
            # 设置画笔颜色为绿色
            painter.setPen(QPen(Qt.green, line_width))
            # 画矩形
            painter.drawRect(
                self.start_x + self.grid_width * self.press_i,
                self.start_y + self.grid_height * self.press_j,
                self.grid_width,
                self.grid_height,
            )
    def mousePressEvent(self, event: QMouseEvent):  # 鼠标按下事件
        # 调用父类同名方法
        super().mousePressEvent(event)
        
        # 获取 x, y 坐标
        x = event.x()
        y = event.y()
        
        # 范围判断，点击范围不要超过棋盘
        if (
            x >= self.start_x
            and x <= self.start_x + self.grid_width * 8
            and y >= self.start_y
            and y <= self.start_y + self.grid_height * 8
        ):
            temp_i = (x - self.start_x) // self.grid_width
            temp_j = (y - self.start_y) // self.grid_height
            # print(temp_i, temp_j)
            
            # 发送信号
            self.signal_mouse_press.emit(temp_i, temp_j)
            
    def set_chess_info(self, chess, x, y):
        """设置棋盘棋子状态"""
        self.chess = chess
        
        self.press_i = x
        self.press_j = y
        
        self.update()


