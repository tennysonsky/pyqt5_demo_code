import sys

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from ui.Ui_main_widget import Ui_MainWidget


class MyWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_MainWidget()
        self.ui.setupUi(self)
        # 设置窗口最小大小
        self.setMinimumSize(900, 800)
        
        # 实例属性
        self.grid_width = 0 # 格子宽度
        self.grid_height = 0 # 格子高度
        self.start_x = 0 # 棋盘起点x坐标
        self.start_y = 0 # 棋盘起点y坐标

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        pass
    
    def paintEvent(self, event: QPaintEvent): # 绘图事件
        # 调用父类同名方法
        super().paintEvent(event)
        
        # 定义画家（请了一个画家来画画），画在主窗口上
        painter = QPainter(self)
        
        # 以窗口大小画背景图
        painter.drawPixmap(0, 0, self.width(), self.height(), 
                            QPixmap('./img/board.jpg') )
        
        # 窗口宽度分12份
        self.grid_width = self.width() // 12
        # 窗口高度分12份
        self.grid_height = self.height() // 12
        
        # 棋盘起点坐标为grid_width， grid_height
        self.start_x = self.grid_width*2
        self.start_y = self.grid_height*2
        
        # 设置线宽为2
        line_width = 4
        painter.setPen(QPen(Qt.black, line_width))
        
        # 取中间8份画棋盘
        for i in range(9):
            # 画横线
            painter.drawLine(self.start_x, self.start_y + self.grid_height * i, 
                             self.start_x + self.grid_width * 8, self.start_y + self.grid_height * i)
            # 画竖线
            painter.drawLine(self.start_x + self.grid_width * i, self.start_y, 
                             self.start_x + self.grid_width * i, self.start_y + self.grid_height * 8)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.show()

    sys.exit(app.exec_())
