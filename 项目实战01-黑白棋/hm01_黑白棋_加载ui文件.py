import sys

from PyQt5.QtWidgets import *

from ui.Ui_main_widget import Ui_MainWidget


class MyWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_MainWidget()
        self.ui.setupUi(self)

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        pass


if __name__ == '__main__':
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.show()

    sys.exit(app.exec_())
