"""
以下导入方式会导致允许冲突

导入格式2：     from 模块名 import 需使用的函数、类、变量
使用格式：     函数、类、变量   无需通过模块名引用

导入格式3：     from 模块名 import *
使用格式：     函数、类、变量   无需通过模块名引用
"""

from utils import name, sum
from demo import name, sum  # 名字冲突，只有最后的导入名字才有效

print(name)
print(sum(1, 2))



