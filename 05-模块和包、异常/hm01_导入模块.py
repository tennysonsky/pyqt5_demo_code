"""
导入格式1：     import 模块名
使用格式：     模块名.函数  模块名.类名  模块名.变量名

导入格式2：     from 模块名 import 需使用的函数、类、变量
使用格式：     函数、类、变量   无需通过模块名引用

导入格式3：     from 模块名 import *
使用格式：     函数、类、变量   无需通过模块名引用
"""
print("===============import 模块===================")

import utils

print(utils.sum(1, 2))
print(utils.name)

p = utils.Person('张三', 18)
print(p)

# print("======from 模块名 import 需使用的函数、类、变量========")
# from utils import sum, name, Person
#
# sum(1, 2)
# print(name)
# p = Person('张三', 18)
# print(p)

print("===============from 模块名 import *===================")
from utils import *

sum(1, 2)
print(name)
p = Person('张三', 18)
print(p)
