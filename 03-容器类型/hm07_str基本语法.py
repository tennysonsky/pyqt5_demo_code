"""
# 1. 字符串定义：一对引号(单、双、三)包含
# 2. 获取字符串中元素：字符串[索引]
# 3. 遍历字符串
"""
# 1. 字符串定义：一对引号(单、双、三)包含
data = 'hello'
# data = "hello"
# data = """hello"""
print(data, type(data))
# 2. 获取字符串中元素：字符串[索引]
print(data[-1])
# 3. 遍历字符串
for v in data:
    print(v)