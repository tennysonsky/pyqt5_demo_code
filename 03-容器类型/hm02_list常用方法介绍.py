"""
增：
    列表.append(元素)
删：
    列表.pop(元素索引)
    列表.remove(元素)
改：
    列表[索引] = 新内容
查：
    列表[索引]
找：
    列表.index(元素) 返回元素所在的位置
排序：
    升序：列表.sort()
    降序：列表.sort(reverse=True)
反转：
    列表.reverse()
"""
name_list = ["柯南", "多啦A梦", "鸣人", "亚索"]

# 添加元素
name_list.append("佐助")
name_list.insert(0, "哈哈哥")
print(name_list)

# 修改元素
name_list[0] = "工藤新一"
print(name_list)

# 查询指定位置
print(name_list[2])
# 根据元素内容，得到其索引
i = name_list.index("多啦A梦")
print(i, type(i))

# 删除元素
name_list.remove("亚索")  # 元素内容
# 使用pop，不写索引参数，默认移除末尾元素
# name_list.pop(1)  # 元素索引
del name_list[1]  # 删除指定位置元素
print(name_list)

# 排序，数组本身变化
# 正序排列，从小到大
arr = [14, 54, 12, 2, 3, 44, 123]
arr.sort()  # [2, 3, 12, 14, 44, 54, 123]
# 倒序排列
arr = [14, 54, 12, 2, 3, 44, 123]
arr.sort(reverse=True)  # [123, 54, 44, 14, 12, 3, 2]
# arr.reverse() # 把列表进行倒序排列
