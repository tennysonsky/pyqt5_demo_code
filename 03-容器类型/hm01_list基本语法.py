"""
# 1. 列表定义 列表变量的名字 = [元素1, 元素2, ……]
# 2. 列表元素访问 列表[索引]
# 3. 列表遍历
"""
# 1. 列表定义 列表变量的名字 = [元素1, 元素2, ……]
name_list = ['林青霞', '张曼玉', '胡慧中']
print(name_list, type(name_list))

# 2. 列表元素访问 列表[索引]
print(name_list[1])

# 3. 列表遍历
for name in name_list:
    print(name)
