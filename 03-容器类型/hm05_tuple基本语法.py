"""
1. 元组定义：
    元组变量 = (元素1, 元素2, ……)
2. 元组只有一个元素，定义格式:
    元组变量 = (元素,)
3. 元组的元素只读，不能改
"""

# 1. 元组定义
num_tuple = (1, 2, 3, 4)
print(num_tuple, type(num_tuple))

# 2. 元组只有一个元素，定义格式
num_tuple2 = (250,)

# 3. 元组的元素只读，不能改
# num_tuple[-1] = 100 # TypeError: 'tuple' object does not support item assignment
