"""
1. 遍历字典的键
# for 任意变量 in 字典变量：
for 任意变量 in 字典变量.keys(): # 和上面等价
    任意变量保存的是字典的键

2. 遍历字典的值
for 任意变量 in 字典变量.values():
    任意变量保存的是字典的值

3. 遍历字典的键和值
for 任意变量1, 任意变量2 in 字典变量.items():
    任意变量1保存的是字典的键
    任意变量2保存的是字典的值
"""

d = {
    '中国': 'China',
    '英国': 'England',
    '美国': 'America'
}

print("-----------------------1")
# 直接遍历
for key in d:
    print(key, d[key])

print("-----------------------2")
# 遍历key
for key in d.keys():
    print(key, d[key])

print("-----------------------3")
for value in d.values():
    print(value)

print("-----------------------4")
for k, v in d.items():
    print(k, v)
