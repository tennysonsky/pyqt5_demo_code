"""
需求：
● 用户名和密码格式校验程序
● 要求从键盘输入用户名和密码，分别校验格式是否符合规则
    ○ 如果符合，打印用户名合法，密码合法
    ○ 如果不符合，打印出不符合的原因，并提示重新输入
● 用户名长度6-20，用户名必须以字母开头
● 密码长度至少6位，不能为纯数字，不能有空格

分析：
1.从键盘输入用户名(需要while循环)
2.长度6-20
3.必须字母开头

4.输入密码(while循环)
5.密码长度至少6位
6.不能为纯数字
7.不能有空格
"""
while True:
    user_name = input("请输入用户名：")
    if not (6 <= len(user_name) <= 20):
        print("用户名长度6-20，请重新输入")
        continue
    elif not user_name[0].isalpha():
        print("必须以字母开头")
        continue
    else:
        print("用户名合法")
        break

while True:
    password = input("请输入密码：")
    if len(password) < 6:
        print("密码长度至少6位", password)
        continue
    elif password.isdecimal():
        print("密码不能是纯数字", password)
        continue
    elif " " in password:
        print("不能有空格", password)
        continue
    else:
        print("密码合法", password)
        break
