"""
# 1. 集合定义： {元素1, 元素2, 元素3}
# 2. 集合作用：去重，元素唯一
# 3. 遍历 for in
# 4. 添加： 集合.add(元素)
# 5. 删除
    集合.remove(元素)   # 元素不存在报错
    集合.pop()    # 随机删除
    集合.discard(元素)  # 删除元素 如果元素不存在,不做任何处理
"""
# 1. 集合定义 {元素1, 元素2, 元素3}
data = {1, 3, 5, 5, 3, 2}
print(data, type(data))

# 2. 集合作用：去重，元素唯一
temp = [1, 3, 5, 5, 3, 1]
temp = list(set(temp))
print(temp)

# 3. 遍历 for in
for v in data:
    print(v)

# 4. 添加： 集合.add(元素)
data.add(7)
print(data)

# 5. 删除
data.discard(5)
print(data)
