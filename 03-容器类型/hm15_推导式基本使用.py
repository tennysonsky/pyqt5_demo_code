"""
推导式指的是轻量级循环创建数据的方式，对列表或可迭代对象中的每个元素应用某种操作，用生成的结果创建新的列表；
或用满足特定条件的元素创建子序列。
推导式包括：
● 列表推导式
● 元组推导式
● 集合推导式
● 字典推导式
格式：

[计算公式 for循环 if判断]
# 1. 通过for循环遍历所有元素
# 2. 符合后边if判断,才保留下来！
# 3. 根据前边公式保留结果

# zip把两个列表组合成一个，每个元素包括两个一一对应的元素
# zip(..., ...) 将range(1,10)和range(21,30)里的每个元素一一组合成元组
# rst = zip(range(1, 10), range(21, 30))
"""
print("==================列表推导式=======================")
# 1. 通过for循环遍历所有元素
# 2. 符合后边if判断,才保留下来！
# 3. 根据前边公式保留结果
lst = [i for i in range(1, 11) if i % 2 == 0]
print(lst)

lst = [i ** 2 for i in range(1, 11) if i % 2 == 0]
print(lst)

print("==================元组推导式=======================")
t = (i for i in range(1, 11) if i % 2 == 0)
print(t, tuple(t))

t = (i ** 2 for i in range(1, 11) if i % 2 == 0)
print(t, tuple(t))

print("==================集合推导式=======================")
s = {i for i in range(1, 11) if i % 2 == 0}
print(s, type(s))

s = {i ** 2 for i in range(1, 11) if i % 2 == 0}
print(s, type(s))

print("==================字典推导式=======================")
d = {k: k * 2 for k in range(1, 11) if k % 2 == 0}
print(d, type(d))

# zip把两个列表组合成一个，每个元素包括两个一一对应的元素
rst = zip(range(1, 10), range(21, 30))
# zip类型的数据，需要重复读取，需要提前做转换
aa = list(rst)
print(aa)

dd = {key: value for key, value in zip(range(1, 10), range(21, 30))}
print(dd)
