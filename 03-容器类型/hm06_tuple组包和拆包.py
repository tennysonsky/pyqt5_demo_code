"""
元组的组包(多变一)：多个数据组装成一个元组，右边可以不用写()
    元组变量 = 数据1，数据2，数据3
元组的拆包(一变多)：元组变量的元素拆分为多个元素
    变量1, 变量2, 变量3 = 元组变量

需求：通过元组的组包拆包特点交换2个数的值
"""
# 组包
num_tuple = 10, 20, 30
print(num_tuple, type(num_tuple))

print('=' * 50)
# 拆包
a, b, c = num_tuple
print(a, b, c)

print('=' * 50)
# 需求：通过元组的组包拆包特点交换2个变量的值
num1 = 11
num2 = 22

# num1, num2 = num2, num1 # 简写方式，过程如下
temp = num2, num1  # 组包： temp = (22, 11)
num1, num2 = temp  # 拆包： num1, num2 = (22, 11)
print(num1, num2)