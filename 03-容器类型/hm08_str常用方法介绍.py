"""
判断：
    字符串.isalpha()	如果 string 至少有一个字符并且所有字符都是字母则返回 True
    字符串.isdecimal()	如果 string 只包含数字则返回 True
    字符串.startwith(str)	检查字符串是否是以 str 开头，是则返回 True
    字符串.endswith(str)	检查字符串是否是以 str 结束，是则返回 True
查找：
    字符串.find(指定内容) # 从左往右找，找到返回指定内容第一个元素位置，找不到返回-1
    字符串.rfind(指定内容) # 从右往左找，找到返回指定内容第一个元素位置，找不到返回-1
替换：
    字符串.replace(旧内容, 新内容, 替换次数(不写此值，默认全部替换))
    说明：原来的字符串没有改变，返回值才是替换后的新字符串
切割：
    字符串.split(切割符号，maxsplit=切割次数)
    说明：原来的字符串没有改变，返回值为切割后内容，为列表类型，列表保存切割后内容
去空白
    字符串.strip()
    说明：原来的字符串没有改变，返回值才是去空白后的新字符串
"""
print("-----------------------------判断")
print("abcefg".isalpha())
print("abcefg123".isalpha())
print("123456".isdecimal())
print("123456abc".isdecimal())
print("hello".startswith("he"))
print("hello".endswith("lo"))

print("-----------------------------查找")
print("abcdef".find("d"))
print("itheima.com".rfind("."))

com = "itheima.com"
print(com.replace("hei", "bai"))

print("-----------------------------替换")
# 替换指定个内容
com = "itheima.com.itcast"
print(com.replace("i", "I", 2))

print("-----------------------------切割")
# 参数二：  maxsplit + 1
com = "itheima.com.itcast"
print(com.split(".", maxsplit=1))
print(com.split("."))

print("-----------------------------去空白")
print("   a12345 ")
print("   a12345 ".strip())
