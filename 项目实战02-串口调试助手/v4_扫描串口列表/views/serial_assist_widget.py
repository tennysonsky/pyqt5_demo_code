import sys
from PyQt5.QtWidgets import *

# 帮我们直接运行此文件时，可以加载到上级目录的ui包
# 注意，需要放在导入ui包的前面
sys.path.append("../")

from ui.Ui_serial_assist_widget import Ui_SerialAssistWidget
from views.serial_setting_dialog import SerialSettingDialog
from drivers.driver_serial import *


class SerialAssistWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_SerialAssistWidget()
        self.ui.setupUi(self)
        
        # 实例属性
        self.devices = []

        # 初始化UI界面
        self.init_ui()
        
        # 扫描设备
        self.refresh_devices()

    def init_ui(self):
        # 按钮信号和槽函数绑定
        self.ui.btn_setting.clicked.connect(self.btn_setting_slot) # 设置按钮
        self.ui.btn_refersh.clicked.connect(self.refresh_devices)  # 刷新按钮

    def refresh_devices(self):
        # 刷新设备列表
        print("刷新设备列表")
        self.devices = scan_serial_ports()
        print(self.devices)
        ports_names = [items[1] for items in self.devices]
        print(ports_names)
        # 清空下拉框列表
        self.ui.cb_device.clear()
        # 添加选项
        self.ui.cb_device.addItems(ports_names)

    def btn_setting_slot(self):
        print("设置按钮被点击")
        """设置按钮的槽函数"""
        # 打开设置对话框
        dialog = SerialSettingDialog()
        
        # 对话框信号和槽连接
        dialog.setting_signal.connect(self.setting_dialog_slot)

        dialog.exec_()

    def setting_dialog_slot(self, baudrate, data_bits):
        print(baudrate, data_bits)
        # 设置下拉框内容
        self.ui.cb_baud.setCurrentIndex(self.ui.cb_baud.findText(baudrate))
        
if __name__ == "__main__":
    app = QApplication(sys.argv)

    w = SerialAssistWidget()
    w.show()

    sys.exit(app.exec_())
