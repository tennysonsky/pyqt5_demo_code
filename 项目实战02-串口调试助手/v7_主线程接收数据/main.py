import sys
from PyQt5.QtWidgets import *
from ui.Ui_main_window import Ui_MainWindow


class MainWindow(QMainWindow):
    """注意：父类必须是QMainWindow"""
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        pass


if __name__ == '__main__':
    app = QApplication(sys.argv)

    W = MainWindow()
    W.show()

    sys.exit(app.exec_())