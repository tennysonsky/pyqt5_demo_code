import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

# 帮我们直接运行此文件时，可以加载到上级目录的ui包
# 注意，需要放在导入ui包的前面
sys.path.append("../")

from ui.Ui_bluetooth_assist_widget import Ui_BleAssistWidget
from drivers.driver_bluetooth import *


class BleAssistWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_BleAssistWidget()
        self.ui.setupUi(self)
        
        # 初始化UI界面
        self.init_ui()
        
        
    def refresh_devices(self):
        print("刷新设备列表")
        print("=========================================开始扫描")
        devices = BluetoothDataTransfer.scan_devices()
        # for device in devices:
        #     print(device)
        print([device[1] for device in devices])
        print("=========================================结束扫描")

    def btn_refresh_slot(self):
        print("刷新设备按钮槽函数")
        self.refresh_devices()
    

    def init_ui(self):
        # 按钮信号和槽函数绑定
        self.ui.btn_refresh.clicked.connect(self.btn_refresh_slot)

        
if __name__ == "__main__":
    app = QApplication(sys.argv)

    w = BleAssistWidget()
    w.show()

    sys.exit(app.exec_())
