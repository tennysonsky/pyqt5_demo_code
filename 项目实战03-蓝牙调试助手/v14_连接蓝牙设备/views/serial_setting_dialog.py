import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

# 帮我们直接运行此文件时，可以加载到上级目录的ui包
# 注意，需要放在导入ui包的前面
sys.path.append("../")

from ui.Ui_serial_setting_dialog import Ui_Dialog

"""
1. 定义信号：在类中定义一个信号，使用PyQt的信号机制可以实现自定义信号。可以通过pyqtSignal()方法来创建一个信号对象。 
2. 连接信号和槽：使用connect()方法将信号和槽连接起来。 
3. 触发信号：在需要触发信号的地方，使用emit()方法来触发该信号。 
"""

class SerialSettingDialog(QDialog):
    """继承于QDialog"""
    
    setting_signal = pyqtSignal(str, str)
    
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        
        # 实例属性
        self.baudrate = None    # 波特率
        self.data_bits = None   # 数据位

        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        # 按钮的信号和槽函数绑定
        self.ui.btnOk.clicked.connect(self.btn_ok_slot)
        self.ui.btnCancel.clicked.connect(self.close)
        
    def btn_ok_slot(self):
        # ok按钮槽函数
        print('ok')
        # 读取当前的设置值
        self.baud_rate = self.ui.cb_bt.currentText()
        self.data_bits = self.ui.cb_data.currentText()
        # print(self.baud_rate, self.data_bits)
        
        # 信号发送
        self.setting_signal.emit(self.baud_rate, self.data_bits)
        
        self.close() # 窗口关闭


if __name__ == '__main__':
    app = QApplication(sys.argv)

    w = SerialSettingDialog()
    w.show()

    sys.exit(app.exec_())
