import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

# 帮我们直接运行此文件时，可以加载到上级目录的ui包
# 注意，需要放在导入ui包的前面
sys.path.append("../")

from ui.Ui_bluetooth_assist_widget import Ui_BleAssistWidget


class BleAssistWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        # 实例化ui模块对象，调用setupUi()方法
        self.ui = Ui_BleAssistWidget()
        self.ui.setupUi(self)
        
        # 初始化UI界面
        self.init_ui()

    def init_ui(self):
        pass
        
if __name__ == "__main__":
    app = QApplication(sys.argv)

    w = BleAssistWidget()
    w.show()

    sys.exit(app.exec_())
