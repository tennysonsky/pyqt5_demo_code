"""
1. self是什么？
    谁调用方法，self就是那个谁
    tom对象调用eat方法，self就是tom对象
2. 如何访问属性和方法
    类方法里面： self.属性、self.方法()
    类外面：    对象变量名.属性、对象变量名.方法()
"""


class Person(object):
    """人类"""

    def run(self):
        print(f"{self.name}爱运动")


# 1. 实例化对象
tom = Person()
# 2. 添加属性
tom.name = '汤姆'
# 3. 调用方法
tom.run()

# 2. 重新实例化一个对象，重复上面操作
xm = Person()
xm.name = '小明'
xm.run()
