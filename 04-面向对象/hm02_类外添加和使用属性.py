"""
在类的外面操作对象属性：
1. 创建对象变量(实例化对象)
2. 添加属性：对象.属性 = 内容
    注意：
    1. 第一赋值叫添加属性，下一次赋值叫修改属性
    2. 类外添加属性，了解即可
3. 使用属性：对象变量.属性
"""


class Person(object):
    """人类"""

    def eat(self):
        print("吃饭")

    def run(self):
        print("运动")


# 1. 创建对象变量(实例化对象)
tom = Person()
# 2. 添加属性：对象.属性 = 内容
tom.name = '汤姆'
# 3. 使用属性：对象变量.属性
print(tom.name)
# 4. 修改属性，再打印属性
tom.name = 'mike'
print(tom.name)
