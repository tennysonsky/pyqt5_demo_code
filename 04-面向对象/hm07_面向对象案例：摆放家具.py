"""
需求：
1. 房子(House) 有 户型、 总面积、剩余面积 和 家具名称列表
    - 新房子没有任何的家具、剩余面积和总面积相等
2. 家具(HouseItem) 有 家具名字 和 家具面积， 其中
    - 席梦思(bed) 占地 4 平米
    - 衣柜(chest) 占地 2 平米
    - 餐桌(table) 占地 1.5 平米
3. 将以上三件 家具 添加 到 房子 中
    - 向房间 添加家具 时， 让 剩余面积 -= 家具面积
4. 打印房子时， 要求输出： 户型、 总面积、 剩余面积、 家具名称列表

家具类
类名：HouseItem
属性：家具名称name、家具面积area
方法：
    __init__(): 添加属性
    __str__(): 以字符串格式返回属性信息


房子类
类名：House
属性：户型house_type、 总面积area、剩余面积free_area 和 家具名称列表item_name_list
方法：
    __init__(): 添加属性
    __str__(): 以字符串格式返回属性信息
    add_item(): 添加家具，需要一个形参house_item接收家具对象
"""


class HouseItem(object):
    """家具类"""

    def __init__(self, name, area):
        self.name = name  # 家具名称
        self.area = area  # 家具面积

    def __str__(self):
        return f"[{self.name}]占地面积： {self.area}"


class House(object):
    """房子类"""

    def __init__(self, house_type, area):
        self.house_type = house_type  # 户型
        self.area = area  # 总面积
        self.free_area = area  # 剩余面积
        self.item_name_list = []  # 家具名称列表

    def __str__(self):
        return (f"户型:{self.house_type} "
                f"总面积:{self.area} "
                f"剩余面积:{self.free_area} "
                f"家具名称列表:{self.item_name_list}")

    def add_item(self, house_item):
        # house_item 代表家居对象
        # 判断家具面积是否大于房子剩余面积
        if house_item.area > self.free_area:
            print(f"{house_item.name}的面积太大了， 不能添加到房子中！ ")
        else:
            # 打印对象，对象的位置替换为__str__的返回值
            print(f"添加了{house_item}")
            # 向房间 添加家具
            self.item_name_list.append(house_item.name)
            #  剩余面积 -= 家具面积
            self.free_area -= house_item.area


# 实例化家具对象
bed = HouseItem('席梦思', 4)
chest = HouseItem('衣柜', 2)
table = HouseItem('餐桌', 1.5)

# 实例化对象房子
house = House('小户型', 60)
house.add_item(bed)  # 房子添加床
print(house)  # 输出房子的属性信息
house.add_item(chest)  # 房子添加衣柜
print(house)  # 输出房子的属性信息
house.add_item(table)  # 房子添加餐桌
print(house)  # 输出房子的属性信息
