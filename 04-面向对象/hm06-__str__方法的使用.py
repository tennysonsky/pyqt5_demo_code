"""
__str__方法:
    0. 作用：方便打印对象的属性信息
    1. 返回值必须是字符串类型，不是返回print
    2. print(对象变量名)  对象变量名的位置替换为__str__()方法返回值的内容
"""

class Person(object):
    def __init__(self, name, age):
        print('__init__')
        self.name = name
        self.age = age

    def eat(self):
        """吃东西方法"""
        print(f'{self.name}爱吃饭, 今年 {self.age} 岁')  # self.属性名

    def __str__(self):
        return f'{self.name}爱吃饭, 今年 {self.age} 岁'


# 实例化对象
tom = Person('汤姆', 20)
# 没有实现__str__前，直接打印对象变量，会返回对象在内存中的地址信息
print(tom)
