"""
1. 多态指的是一类事物有多种形态（一个类有多个子类）
2. 多态的概念依赖于继承

需求：
1. 中国人ZhHuman、美国人UsHuman、非洲人AfricaHuman都是属于Human人类的子类
2. 对于Human来说有多个子类就称为多态
3. 每个类都有eat方法，不同类不同表现
"""


class Human:

    def eat(self):
        print("人类吃饭")


class ZhHuman(Human):

    def eat(self):
        print("中国人拿筷子吃饭")


class UsHuman(Human):

    def eat(self):
        print("美国人拿刀叉吃饭")


class AfricaHuman(Human):

    def eat(self):
        print("非洲人用手吃饭")


def someone_eat(someone):
    someone.eat()


zh_human = ZhHuman()
us_human = UsHuman()
af_human = AfricaHuman()

someone_eat(zh_human)
someone_eat(us_human)
someone_eat(af_human)
