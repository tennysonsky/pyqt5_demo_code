"""
说明: 当属性和方法只需要在类定义内部使用时, 就可以使用私有属性和私有方法

定义格式：
私有属性: __属性名
私有方法: def __方法名():

女孩类：
类名：Girl
实例属性：
    姓名name
    年龄__age
实例方法：
    __init__() 添加实例属性
    __secret() 打印年龄
    use_private() 公共方法
"""


class Girl(object):
    def __init__(self, name):
        self.name = name
        # 不要问女生的年龄
        self.__age = 18

    def __secret(self):
        print(f"我的年龄是 {self.__age}")

    def use_private(self):
        print('公有普通方法')
        print(self.__age)
        self.__secret()


g = Girl("小芳")
# 私有属性， 外部不能直接访问
# print(g.__age)
# 私有方法， 外部不能直接调用
# g.__secret()
g.use_private()
