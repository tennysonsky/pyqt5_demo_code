"""
创建线程：线程对象 = threading.Thread(target=要执行的函数名)
执行线程：线程对象.start()
获取当前执行线程：threading.current_thread()
"""


import time
import threading


def singing():
    print("singing当前执行的线程为：", threading.current_thread())
    for i in range(5):
        print("唱歌...", i)
        time.sleep(0.5)


def dancing():
    print("dancing当前执行的线程为：", threading.current_thread())
    for i in range(5):
        print("跳舞...", i)
        time.sleep(0.5)


if __name__ == '__main__':
    print("主线程：", threading.current_thread())
    t1 = threading.Thread(target=singing)
    t1.start()
    t2 = threading.Thread(target=dancing)
    t2.start()
