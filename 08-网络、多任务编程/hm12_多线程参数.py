"""
# 参数传递方式1：元组，只有一个元素时，不要忘记逗号 (10,)
# 参数传递方式2：字典，不要求顺序
# 参数传递方式3：元组+字典，(元组里的内容不可以和字典里的内容冲突)
"""

import threading
import time


def singing(name, age=18, score=123):
    for i in range(5):
        print("{}({}) 唱歌...".format(name, age), i)
        # print("{} 唱歌... ->".format(name), i)
        time.sleep(0.5)


if __name__ == '__main__':
    # 参数传递方式1：元组，只有一个元素时，不要忘记逗号 (10,)
    # thread = threading.Thread(target=singing, args=("张靓颖",))

    # 参数传递方式2：字典，不要求顺序
    # thread = threading.Thread(target=singing, kwargs={"age": 30, "name": "邓紫棋"})

    # 参数传递方式3：元组+字典，(元组里的内容不可以和字典里的内容冲突)
    t = threading.Thread(target=singing, args=("张靓颖",), kwargs={"age": 30})

    t.start()
