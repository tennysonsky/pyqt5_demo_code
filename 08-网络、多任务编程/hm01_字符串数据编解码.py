"""
# 编码encode(encoding=字符编码)： 字符串转换为bytes类型
# 将字符串"你好"转换为bytes类型，encoding参数表示编码方式，返回对象名为bytes_data
# 打印内容、类型

# 解码decode(encoding=字符编码)：bytes类型转换为字符串类型
# 将bytes类型转换为字符串类型  encoding参数表示编码方式
# 打印转换后的类型和类型
"""
# 将字符串"你好"转换为bytes类型，encoding参数表示编码方式，返回对象名为bytes_data
bytes_data = "你好".encode(encoding="utf-8")
# 打印内容、类型
print(bytes_data, type(bytes_data))

# 将bytes类型转换为字符串类型  encoding参数表示编码方式
str_data = bytes_data.decode(encoding="utf-8")
# 打印转换后的类型和类型
print(str_data, type(str_data))
