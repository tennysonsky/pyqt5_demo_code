"""
# 1. 导入socket模块

# 参数1：地址类型：AddressFamily：AF_INET(IPv4) AF_INET6(IPv6)
# 参数2：协议类型：SOCK_STREAM(TCP) SOCK_DGRAM(UDP)
# 2. 创建socket套接字：套接字对象 = socket.socket(family=地址类型, type=协议类型)

# 3. 建立tcp连接（和服务端建立连接）： 套接字.connect((服务器IP, 服务器端口))
# 4. 开始发送数据（到服务端）：  套接字.send(要发送的数据的字节码)
# 5. 关闭套接字 ： 套接字.close()
"""

# 1. 导入socket模块
import socket

# 2. 创建socket套接字
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# 3. 建立tcp连接（和服务端建立连接）
client_socket.connect(("127.0.0.1", 8080))
# 4. 开始发送数据（到服务端）
# client_socket.send("hello") # 不能直接发字符串，要发字节码
# client_socket.send(b"hello")
# client_socket.send("中文utf-8".encode("utf-8"))
client_socket.send("中文默认编码".encode())
# client_socket.send("中文gbk".encode("gbk"))
# 5. 关闭套接字
client_socket.close()
