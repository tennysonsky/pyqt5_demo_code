"""
设置守护线程：
    线程对象.daemon = True，
    或 在创建线程的时候指定daemon = True
    或 线程对象.setDaemon(True)
注意：在线程启动前设置守护线程
"""

import time
import threading


def singing():
    print("singing当前执行的线程为：", threading.current_thread())
    for i in range(5):
        print("唱歌...", i)
        time.sleep(1)


def dancing():
    print("dancing当前执行的线程为：", threading.current_thread())
    for i in range(5):
        print("跳舞...", i)
        time.sleep(1)


if __name__ == '__main__':
    print("主线程：", threading.current_thread())
    t1 = threading.Thread(target=singing)
    # 设置t1为守护线程
    t1.daemon = True
    t1.start()

    t2 = threading.Thread(target=dancing, daemon=True)
    t2.start()

    time.sleep(2)
    print("主线程执行完毕，退出")
    exit(0)
    print("----------")
