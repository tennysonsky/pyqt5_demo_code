"""
# 0. 导入socket

# 参数1：地址类型：AddressFamily：AF_INET(IPv4) AF_INET6(IPv6)
# 参数2：协议类型：SOCK_STREAM(TCP) SOCK_DGRAM(UDP)
# 1. 创建socket套接字：套接字对象 = socket.socket(family=地址类型, type=协议类型)

# 2. bind绑定ip和port ： 套接字.bind((服务器IP, 服务器端口))
# 如果服务ip是空字符串，代表所有网卡的端口都监听数据

# 3. listen使套接字设置为被动模式： 套接字.listen(指定最大连接数)
# 4. accept等待客户端的链接：客户端套接字, 客户端地址 = 套接字.accept()
# 5. recv/send接收发送数据： 套接字.recv(指定接收最大长度), 套接字.send(要发送的数据的字节码)
# 6. tcp关闭套接字： 套接字.close()
"""

# 0. 导入socket
import socket

# 1. 创建socket套接字：套接字对象 = socket.socket(family=地址类型, type=协议类型)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# 2. bind绑定ip和port ： 套接字.bind((服务器IP, 服务器端口))
# # 如果服务ip是空字符串，代表所有网卡的端口都监听数据
server_socket.bind(("", 8080))

# 3. listen使套接字设置为被动模式： 套接字.listen(指定最大连接数)
server_socket.listen(128)

while True:
    # 4. accept等待客户端的链接：客户端套接字, 客户端地址 = 套接字.accept()
    client_socket, client_addr = server_socket.accept()
    print(f"客户端连接成功，客户端地址为:{client_addr}")

    while True:
        # 5. recv/send接收发送数据： 套接字.recv(指定接收最大长度), 套接字.send(要发送的数据的字节码)
        recv_data = client_socket.recv(1024)
        if recv_data:
            print(f"接收到的数据为:{recv_data.decode('gbk')}")

            client_socket.send("消息已收到".encode("utf-8"))
        else:
            # 6. tcp关闭套接字： 套接字.close()
            client_socket.close()  # 关闭客户端套接字
            print(f"{client_addr}客户端已关闭")
            break

# 循环的外面
server_socket.close()  # 关闭服务器套接字
