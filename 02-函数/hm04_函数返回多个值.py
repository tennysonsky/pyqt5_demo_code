"""
函数可以返回多个结果，返回的结果会自动组包成为一个元组数据
"""


# 函数定义
def calc(a, b):
    """
    求a+b以及a-b的结果
    :param a:
    :param b:
    :return: 结果的元组
    """
    sum = a + b
    sub = a - b

    return sum, sub


# 分别接收和以及差
sum, sub = calc(10, 20)
