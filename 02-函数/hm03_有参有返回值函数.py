"""
函数返回值作用：想使用函数内某个数据, 需要使用 return 关键字返回结果
return关键字：结束函数，也可以同时返回一个结果
函数定义格式：
def 函数名():
    return 结果

函数调用格式：
返回值变量 = 函数名()
"""


# 函数定义
def my_add(a, b):
    """
    功能：2数相加
    :param a: 数字1
    :param b: 数字2
    :return: 2数相加结果
    """
    res = a + b
    return res


# 函数调用
result = my_add(10, 20)
print(result)

